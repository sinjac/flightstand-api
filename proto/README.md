# API Proto definition

## Safety warnings
When developing a new script, always test without a propeller installed. Only install the propeller on the motor when you are confident the script has no bugs and works as intended.

Always follow the user guide provided with the product.

## Description
- Client - the user of the server. May be our GUI, a python script, an Android app, etc...
- Server - the entity that the client needs to connect to. The server handles all the hardware and business logic.
- Core - our software running the server.
- API - the communication protocol between the server and client(s).
- .proto - the API documentation file also used as the source to generate the API stubs in each programming language this API is to be used.

Users download and install the Flight Stand Software. When launching the software, an ElectronJS front-end client application is instantiated, and this ElectronJS application automatically launches the core executable in the background.

The core handles the tasks of connecting to USB hardware, recording data, managing powertrains, managing recorded tests, safety cutoffs, and much more.
The core opens a local web server on port 50051. This webserver exposes the API defined in this .proto. The local web server can serve multiple clients simultaneously, so users that wish to use the API through an external Python script can do so.

The API in this project fully defines the communication between the core and the front-end. This same API can be used by our customers that wish to programmatically interact with our products. This .proto uses the [gRPC framwork](https://grpc.io/). 

The front-end (javascript client running as an ElectronJS application) communicates to the core using this API. The front-end will operate in parallel with your custom Python script.

It is possible to run the core executable alone, without launching the GUI, but the core expects the client to provide a heartbeat for safety reasons (the core will cutoff the motors in the event no client is connected). Without the front-end it becomes the responsibility of the client to generate the heartbeat. See more details in the .proto documentation.

## For users of the .proto
To use the API, you must use the compiled .proto into the language of your choice for your application. For example if you need to communicate with the Flight Stand Software using python, import the generated files from the python folder in this project.

When developing your application, use the content of the .proto as the source of information to correctly use and call the RPCs to interact with the Flight Stand Software. See the examples in this project for more information.

## For developers of the .proto
The content in this section is meant for Tyto Robotics employees. It contains useful technology references and internal style guide we use as reference while designing new features.

### Useful reading material
- https://cloud.google.com/apis/design
- .proto style guide https://developers.google.com/protocol-buffers/docs/style
- https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
- https://www.thoughtworks.com/insights/blog/rest-api-design-resource-modeling
- https://medium.com/google-cloud/building-apis-with-grpc-continued-f53b5a5ab850
- Long-lived RPC's (we use lots of them as Watch RPCs): https://www.youtube.com/watch?v=Naonb2XD_2Q
- Slides for the above: https://static.sched.com/hosted_files/kccna18/d5/2018%20CloudNativeCon%20GRPC%20Long%20And%20Streaming%20RPCs.pdf. Follow the slide links to get extra documentation on GRPC.

### Development notes
- "Over 70% of API methods in the Google APIs repository are standard methods" -> we should aim for a similar ratio
- As per Google API Guidelines, and fact a high proportion of customers are from USA, api uses american english.
- "APIs must use product names approved by the product and marketing teams."
- Because of GUI reactivity requirements, we have both GET/LIST and WATCH functions.
- When calling a WATCH, it responds with the first stream right away, as to get the initial state. This allows avoiding
  calling the GET method just before starting the WATCH, and prevents an out of sync state if it changes between the two.
- Custom actions that cannot be associated with a simple field are done with custom methods. There are other ways, such
  as REST without PUT, but here we stay consistent and use custom methods:
  https://cloud.google.com/apis/design/custom_methods
- Debug-level features start with "debug", while dev-only level features start with "dev". Development features are meant for Tyto Robotics employees only.
- We avoid strange proto features like oneof and maps. Keep it simple, consistent, and developer-friendly.

### Development todo list (for future updates)
- cleanup fields numbering and separate in files.
- try out a doc generator to make online docs.
- adding missing implementation for GetSignal, GetOutput, GetPowertrain rpcs. They are not yet implemented because the GUI did not need them.
- add the UpdateTest rpc to allow users to change the title and description of an already recorded test
- better document the ESC protocols
- better document the WatchSamples (sample rate)


