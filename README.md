# Flight Stand Software API

## Introduction

This project contains the API definition used by the Tyto Robotics Flight Stand Software.

In this project we provide examples and compiled libraries to communicate with the Flight Stand Software using Python, Javascript, and Golang. 

The API uses the [gRPC](https://grpc.io/) framework. More programming languages are supported by the gRPC framework, such as C++, Java, Ruby, but you will need to compile the .proto in your target language, which we cannot offer support.

## Getting started

### Safety warnings
When developing a new script, always test without a propeller installed. Only install the propeller on the motor when you are confident the script has no bugs and works as intended.

Always follow the user guide provided with the product.

### YouTube channel
Have a look at our [YouTube channel](https://www.youtube.com/@tytorobotics/playlists) to see usage examples and tutorials for this API.

### Using the correct version
At this time no backward compatibility is guaranteed between versions. Make sure you are using the correct version of the API.

For most users, it is recommended to use the [latest version of this API](https://gitlab.com/TytoRobotics/flightstand-api/-/tree/main) which is compatible with the [latest version of the Flight Stand Software](https://www.tytorobotics.com/blogs/manuals-and-datasheets/rcbenchmark-software-overview).

For [earlier releases of the Flight Stand Software](https://www.tytorobotics.com/blogs/manuals-and-datasheets/flight-stand-software-changelog-and-previous-releases), use the [tags](https://gitlab.com/TytoRobotics/flightstand-api/-/tags) to find the correct version of the API.

### Updating the Flight Stand Software
When we release a new version of the Flight Stand Software, you must update the API in your application. You can learn about the changes between different versions of the API by looking at the [compare report](https://gitlab.com/TytoRobotics/flightstand-api/-/compare?from=main&to=main).

### Run the example project
Note: at this time we only have a complete Python example. Javascript and Golang examples are preliminary.

1) The Flight Stand Software must be running in order for the API to work. You can minimize the software to run in the background if you wish.
2) Select the programming language you will be using to communicate with the API, for example if you pick Python, download the content of the [/languages/python](/languages/python) folder into your project.
3) Follow the setup instructions for the language you will be using in the corresponding README.md. For Python: [/languages/python/README.md](/languages/python/README.md)
4) Run the example project. For example in Python, run the [/languages/python/examples/example.py](/languages/python/examples/example.py) file. You should see various statements printed in the console.

### Develop your own script
Once the example script runs, you may start to either modify a copy of the example, or start from a blank script. Here are some tips:
- Use the [/proto/flight_stand_api_v1.proto](/proto/flight_stand_api_v1.proto) file as the reference to learn the proper commands to achieve your objectives. Use the example script as a guide to convert RPC calls into the proper format specific to your language.
- Not everything needs to be automated in code. Our front-end GUI can be used for many tasks related to preparing the powertrain mappings, setting up safety limits, etc...
- The front-end is fully responsive, so if your script changes something, that will be automatically reflected in the GUI. If you want your script to be responsive to changes done one the GUI, use "watch" RPCs or regularly call "get" or "list" methods.
- Search online. gRPC is meant to be a universal framework, meaning the online community created a vast amount of tutorials and help resources to work with gRPC APIs. Searching online will solve most issues you encounter using the framework and specific language usage. The gRPC team published language specific gRPC guides. For example, here is the official [gRPC quickstart guide for Python](https://grpc.io/docs/languages/python/quickstart/).
- Be careful of hard-coding signal numbers, as these will change depending on the order hardware is connected. Instead look at the list of inputs, and extract the correct signal name from there.

### Connect to the Flight Stand Software from another computer on the network
To interface with the Flight Stand Software from a different computer within the network, you should be aware of certain security aspects. By default, the Flight Stand Software API only accepts local connections to ensure that remote control of the software is intentional. However, starting with version 1.9.1, remote connections can be enabled by initiating the Flight Stand Software with the "--remote" command line argument. 

**Security caution:** currently, there's no mechanism to limit remote access exclusively to particular devices. This implies that any user aware of the computer's IP address can connect to the Flight Stand Software. Therefore, this feature should only be utilized in controlled environments.

You can also see [this 3 minutes video](https://www.youtube.com/watch?v=kfvCMsBG8mg) for instructions.

ON THE COMPUTER WITH THE FLIGHT STAND HARDWARE (TO BE CONTROLLED):
1) In Windows, right-click on the Flight Stand Software shortcut typically found here:
C:\Users\myusername\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Flight Stand Software
2) From the context menu, select "Properties"
3) Modify the target field from this:

    "C:\Users\myusername\AppData\Local\Programs\flight-stand-software\Flight Stand Software.exe"

    to this:

    "C:\Users\myusername\AppData\Local\Programs\flight-stand-software\Flight Stand Software.exe" --remote
4) Take note of the computer's IP address. To do so, you can use Windows Command Prompt, and type "ipconfig".

ON ANOTHER COMPUTER THAT WILL REMOTELY CONTROL THE FLIGHT STAND HARDWARE:

Repeat steps 1 to 3, but instead of the "--remote" flag, use "--target=IP:50051", where IP is the IP address of the computer to connect to.

The real-time plots will follow the host computer timestamps, so if the two computers don't have a synchronized time,
the real-time data will appear with a delay. Follow [these steps](https://pureinfotech.com/sync-clock-windows-10/) on both computers to sync their system times.

While the Flight Stand Software runs only on Windows, cross-platform use is possible through the API. For instance, a Windows PC can operate the Flight Stand Software, while a Linux machine connects to the Flight Stand Software using Python. For this scenario, see the section below to learn about headless mode.

### Headless mode
The Flight Stand Software consists of two primary components: the GUI (front-end) and the core (back-end). When users start the Flight Stand Software, they activate the GUI, which then automatically launches the core. However, there may be cases where users would like to launch the GUI separately. For example, running the core without the GUI on one machine, while running the GUI on another.
A safety feature is that the core will auto-terminate if no client connects to it within 25 seconds, or if a connected client loses connection for more than 10 seconds. To remove this limitation, use the --headless command line argument.

The core executable can be found in the installation directory of the Flight Stand Software, usually in this directory:
C:\Users\myusername\AppData\Local\Programs\flight-stand-software\resources\core\Flight Stand Software Core.exe

The core accepts the following command line arguments:

--remote: activates remote mode on the core. Launching the GUI with the --remote flag results in the GUI forwarding this flag to the core.

--userPath: specifies the location to save test data. If not provided, the data will be stored in the same location as the core.exe.

--headless: activates headless mode. In this mode, the core doesn't shut down automatically when no client is connected.


The GUI can also be launched independently of the core, and supports the following command line arguments:

--no-core: launches the GUI without the core. The GUI will then connect to an already running core on the localhost.

--remote: this flag is forwarded to the core when the GUI launches it.

--target=192.168.1.10:50051: launches the GUI without the core and remotely connects to the core at the specified IP address and port.

## Getting help
You can try getting the help of a third-party AI service to help you write code examples, as long as you provide the 
required context. We prepared a [chat GPT conversation with some context ready to use](https://chat.openai.com/share/03928d7f-0e87-446b-8e16-0fb60c88c15e), 
so you can directly ask it to generate a specific script example. For example:
```
Make a script that will generate a continuous sine wave with a configurable period and amplitude and 
use this sine signal to control the Flight Stand output "/boards/simulated_1/outputs/1".
```
Then you can copy and paste the response in your Python editor. The script you ask may not always work, but with chatGPT
you can ask follow-up questions or ask for a different script, which makes it a useful tool to assist you in writing 
complete scripts. For example, the first script it generated contained a bug, so we used the following follow-up prompt:
```
There is a bug in the previous response, since there is no "find_output_by_path" method. Instead use 
the get_output method. Also, if the simulated board does not exist, automatically create it.
```
Then we continued with more follow-up prompts to eventually end up with a working script. Scroll down to the end of [this
chat](https://chat.openai.com/share/321fac4b-38f8-47ed-9742-e84fcac97a62) to see an example of the process. Remember
AI chat tools are not yet capable of handling large amounts of code, so try to keep the script small, or try to decouple
your prompts into smaller problems.

We are here to help. If you run into issues and need customer support, please contact us at [support@tytorobotics.com](mailto:support@tytorobotics.com).

## Common questions
### My RPM seems wrong! What units are you using?
The API is entirely in SI, so the rotation speed is in rad/s. We made that decision to simplify the API calls and insure a consistent script output regardless of the GUI configuration.


