// source: flight_stand_api_v1.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');
goog.object.extend(proto, google_protobuf_duration_pb);
var google_protobuf_field_mask_pb = require('google-protobuf/google/protobuf/field_mask_pb.js');
goog.object.extend(proto, google_protobuf_field_mask_pb);
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');
goog.object.extend(proto, google_protobuf_empty_pb);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Board', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.BoardParameter', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearCutoffRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearCutoffResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearPowertrainRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearPowertrainResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Core', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CoreSignal', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CoreSignalType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CutoffCondition', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.CutoffConditionType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DeleteTestRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevBackupParametersRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevBackupParametersResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DisconnectBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.DisconnectBoardResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ExternalInput', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ExternalOutput', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetCoreRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetInputRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetOutputRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetPowertrainRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetServerStatusRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetSignalRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetTestRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.IdentifyBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.IdentifyBoardResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Input', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.InputType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListBoardsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListBoardsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListInputsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListInputsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListOutputsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListOutputsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListPowertrainsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListPowertrainsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSamplesRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSamplesResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSignalStatsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSignalStatsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSignalsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListSignalsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListTestSamplesRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListTestSamplesResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListTestsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ListTestsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Output', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.OutputProtocolType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.OutputTarget', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.OutputTargetAction', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.OutputType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Powertrain', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.PowertrainSignal', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.PowertrainSignalType', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ResetBoardRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ResetBoardResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Sample', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SampleGroup', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.ServerStatus', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Signal', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SignalStat', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SyncTimestampsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.SyncTimestampsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TareInputsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TareInputsResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.Test', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TestRecorder', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TimedAction', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TimedActionSequence', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TriggerCutoffRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.TriggerCutoffResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateCoreRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateInputRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateOutputRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateTestRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchBoardsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchCoreRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchInputsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchOutputsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchSamplesRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchSamplesResponse', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchSignalsRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest', null, global);
goog.exportSymbol('proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ServerStatus = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ServerStatus, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ServerStatus.displayName = 'proto.tytorobotics.flightstand.v1.ServerStatus';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TestRecorder = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.TestRecorder.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TestRecorder, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TestRecorder.displayName = 'proto.tytorobotics.flightstand.v1.TestRecorder';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListTestSamplesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListTestSamplesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListTestSamplesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListTestSamplesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SampleGroup = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.SampleGroup.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SampleGroup, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SampleGroup.displayName = 'proto.tytorobotics.flightstand.v1.SampleGroup';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Test = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.Test.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Test, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Test.displayName = 'proto.tytorobotics.flightstand.v1.Test';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Core = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.Core.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Core, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Core.displayName = 'proto.tytorobotics.flightstand.v1.Core';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Board = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.Board.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Board, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Board.displayName = 'proto.tytorobotics.flightstand.v1.Board';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Input = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Input, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Input.displayName = 'proto.tytorobotics.flightstand.v1.Input';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.OutputTarget = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.OutputTarget, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.OutputTarget.displayName = 'proto.tytorobotics.flightstand.v1.OutputTarget';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Signal = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Signal, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Signal.displayName = 'proto.tytorobotics.flightstand.v1.Signal';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SignalStat = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SignalStat, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SignalStat.displayName = 'proto.tytorobotics.flightstand.v1.SignalStat';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Output = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.Output.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Output, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Output.displayName = 'proto.tytorobotics.flightstand.v1.Output';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Powertrain = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.Powertrain.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Powertrain, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Powertrain.displayName = 'proto.tytorobotics.flightstand.v1.Powertrain';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.CutoffCondition = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.CutoffCondition, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.CutoffCondition.displayName = 'proto.tytorobotics.flightstand.v1.CutoffCondition';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.TimedActionSequence.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TimedActionSequence, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TimedActionSequence.displayName = 'proto.tytorobotics.flightstand.v1.TimedActionSequence';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TimedAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.TimedAction.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TimedAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TimedAction.displayName = 'proto.tytorobotics.flightstand.v1.TimedAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.OutputTargetAction, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.OutputTargetAction.displayName = 'proto.tytorobotics.flightstand.v1.OutputTargetAction';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.BoardParameter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.BoardParameter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.BoardParameter.displayName = 'proto.tytorobotics.flightstand.v1.BoardParameter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.PowertrainSignal, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.PowertrainSignal.displayName = 'proto.tytorobotics.flightstand.v1.PowertrainSignal';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.CoreSignal = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.CoreSignal, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.CoreSignal.displayName = 'proto.tytorobotics.flightstand.v1.CoreSignal';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.Sample = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.Sample, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.Sample.displayName = 'proto.tytorobotics.flightstand.v1.Sample';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.displayName = 'proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ExternalInput = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ExternalInput, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ExternalInput.displayName = 'proto.tytorobotics.flightstand.v1.ExternalInput';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ExternalOutput = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ExternalOutput, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ExternalOutput.displayName = 'proto.tytorobotics.flightstand.v1.ExternalOutput';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetServerStatusRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetServerStatusRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetServerStatusRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetCoreRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetCoreRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetCoreRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchCoreRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchCoreRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchCoreRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TriggerCutoffRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.displayName = 'proto.tytorobotics.flightstand.v1.TriggerCutoffRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TriggerCutoffResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.displayName = 'proto.tytorobotics.flightstand.v1.TriggerCutoffResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearCutoffRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearCutoffRequest.displayName = 'proto.tytorobotics.flightstand.v1.ClearCutoffRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearCutoffResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearCutoffResponse.displayName = 'proto.tytorobotics.flightstand.v1.ClearCutoffResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SyncTimestampsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.displayName = 'proto.tytorobotics.flightstand.v1.SyncTimestampsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SyncTimestampsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.displayName = 'proto.tytorobotics.flightstand.v1.SyncTimestampsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateCoreRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateCoreRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateCoreRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListBoardsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListBoardsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListBoardsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListBoardsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListBoardsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListBoardsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListBoardsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchBoardsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchBoardsRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchBoardsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DisconnectBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.DisconnectBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DisconnectBoardResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.displayName = 'proto.tytorobotics.flightstand.v1.DisconnectBoardResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ResetBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ResetBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.ResetBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ResetBoardResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ResetBoardResponse.displayName = 'proto.tytorobotics.flightstand.v1.ResetBoardResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.IdentifyBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.IdentifyBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.IdentifyBoardResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.displayName = 'proto.tytorobotics.flightstand.v1.IdentifyBoardResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.displayName = 'proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.displayName = 'proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetInputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetInputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetInputRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetInputRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateInputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateInputRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateInputRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListInputsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListInputsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListInputsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListInputsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListInputsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListInputsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListInputsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchInputsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchInputsRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchInputsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TareInputsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TareInputsRequest.displayName = 'proto.tytorobotics.flightstand.v1.TareInputsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TareInputsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TareInputsResponse.displayName = 'proto.tytorobotics.flightstand.v1.TareInputsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSamplesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSamplesRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListSamplesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSamplesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSamplesResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListSamplesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchSamplesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchSamplesRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchSamplesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.WatchSamplesResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchSamplesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchSamplesResponse.displayName = 'proto.tytorobotics.flightstand.v1.WatchSamplesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.displayName = 'proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.displayName = 'proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetSignalRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetSignalRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetSignalRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSignalsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSignalsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListSignalsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListSignalsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSignalsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSignalsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListSignalsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchSignalsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchSignalsRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchSignalsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetOutputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetOutputRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetOutputRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateOutputRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateOutputRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateOutputRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListOutputsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListOutputsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListOutputsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListOutputsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListOutputsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListOutputsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListOutputsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchOutputsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchOutputsRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchOutputsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.displayName = 'proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.displayName = 'proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetPowertrainRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetPowertrainRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetPowertrainRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListPowertrainsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListPowertrainsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListPowertrainsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListPowertrainsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearPowertrainRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.displayName = 'proto.tytorobotics.flightstand.v1.ClearPowertrainRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearPowertrainResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.displayName = 'proto.tytorobotics.flightstand.v1.ClearPowertrainResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.displayName = 'proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.displayName = 'proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.displayName = 'proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.displayName = 'proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.displayName = 'proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevBackupParametersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.displayName = 'proto.tytorobotics.flightstand.v1.DevBackupParametersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevBackupParametersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.displayName = 'proto.tytorobotics.flightstand.v1.DevBackupParametersResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.displayName = 'proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.displayName = 'proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.displayName = 'proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.displayName = 'proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.displayName = 'proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.displayName = 'proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.displayName = 'proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.displayName = 'proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.GetTestRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.GetTestRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.GetTestRequest.displayName = 'proto.tytorobotics.flightstand.v1.GetTestRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.UpdateTestRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.UpdateTestRequest.displayName = 'proto.tytorobotics.flightstand.v1.UpdateTestRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListTestsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListTestsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListTestsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListTestsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListTestsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListTestsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListTestsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.DeleteTestRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.DeleteTestRequest.displayName = 'proto.tytorobotics.flightstand.v1.DeleteTestRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSignalStatsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.displayName = 'proto.tytorobotics.flightstand.v1.ListSignalStatsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.repeatedFields_, null);
};
goog.inherits(proto.tytorobotics.flightstand.v1.ListSignalStatsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.displayName = 'proto.tytorobotics.flightstand.v1.ListSignalStatsResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ServerStatus.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ServerStatus} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ServerStatus.toObject = function(includeInstance, msg) {
  var f, obj = {
    clientsCount: jspb.Message.getFieldWithDefault(msg, 1, 0),
    tokenUuid: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ServerStatus}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ServerStatus;
  return proto.tytorobotics.flightstand.v1.ServerStatus.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ServerStatus} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ServerStatus}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setClientsCount(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setTokenUuid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ServerStatus.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ServerStatus} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ServerStatus.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getClientsCount();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getTokenUuid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 clients_count = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.getClientsCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ServerStatus} returns this
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.setClientsCount = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string token_uuid = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.getTokenUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ServerStatus} returns this
 */
proto.tytorobotics.flightstand.v1.ServerStatus.prototype.setTokenUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.TestRecorder.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TestRecorder.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TestRecorder} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TestRecorder.toObject = function(includeInstance, msg) {
  var f, obj = {
    isContinuousRecording: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    title: jspb.Message.getFieldWithDefault(msg, 2, ""),
    manualSamplesCount: jspb.Message.getFieldWithDefault(msg, 3, 0),
    segment: (f = msg.getSegment()) && proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.toObject(includeInstance, f),
    continuousSegmentsList: jspb.Message.toObjectList(msg.getContinuousSegmentsList(),
    proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TestRecorder;
  return proto.tytorobotics.flightstand.v1.TestRecorder.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TestRecorder} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsContinuousRecording(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setTitle(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setManualSamplesCount(value);
      break;
    case 4:
      var value = new proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinaryFromReader);
      msg.setSegment(value);
      break;
    case 5:
      var value = new proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinaryFromReader);
      msg.addContinuousSegments(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TestRecorder.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TestRecorder} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TestRecorder.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIsContinuousRecording();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getTitle();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getManualSamplesCount();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getSegment();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.serializeBinaryToWriter
    );
  }
  f = message.getContinuousSegmentsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.serializeBinaryToWriter
    );
  }
};


/**
 * optional bool is_continuous_recording = 1;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.getIsContinuousRecording = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.setIsContinuousRecording = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional string title = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.getTitle = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.setTitle = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int32 manual_samples_count = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.getManualSamplesCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.setManualSamplesCount = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional ContinuousRecordingSegment segment = 4;
 * @return {?proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.getSegment = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, 4));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
*/
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.setSegment = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.clearSegment = function() {
  return this.setSegment(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.hasSegment = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated ContinuousRecordingSegment continuous_segments = 5;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.getContinuousSegmentsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, 5));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>} value
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
*/
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.setContinuousSegmentsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment}
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.addContinuousSegments = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.TestRecorder} returns this
 */
proto.tytorobotics.flightstand.v1.TestRecorder.prototype.clearContinuousSegmentsList = function() {
  return this.setContinuousSegmentsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    testName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    continuousRecording: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    continuousSegmentIndex: jspb.Message.getFieldWithDefault(msg, 3, 0),
    resampleStepDuration: (f = msg.getResampleStepDuration()) && google_protobuf_duration_pb.Duration.toObject(includeInstance, f),
    pageSize: jspb.Message.getFieldWithDefault(msg, 5, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 6, ""),
    lowPassFilterCutoffHz: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListTestSamplesRequest;
  return proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setTestName(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setContinuousRecording(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setContinuousSegmentIndex(value);
      break;
    case 4:
      var value = new google_protobuf_duration_pb.Duration;
      reader.readMessage(value,google_protobuf_duration_pb.Duration.deserializeBinaryFromReader);
      msg.setResampleStepDuration(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLowPassFilterCutoffHz(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTestName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getContinuousRecording();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getContinuousSegmentIndex();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getResampleStepDuration();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_duration_pb.Duration.serializeBinaryToWriter
    );
  }
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getLowPassFilterCutoffHz();
  if (f !== 0.0) {
    writer.writeFloat(
      7,
      f
    );
  }
};


/**
 * optional string test_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getTestName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setTestName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool continuous_recording = 2;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getContinuousRecording = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setContinuousRecording = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional int32 continuous_segment_index = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getContinuousSegmentIndex = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setContinuousSegmentIndex = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional google.protobuf.Duration resample_step_duration = 4;
 * @return {?proto.google.protobuf.Duration}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getResampleStepDuration = function() {
  return /** @type{?proto.google.protobuf.Duration} */ (
    jspb.Message.getWrapperField(this, google_protobuf_duration_pb.Duration, 4));
};


/**
 * @param {?proto.google.protobuf.Duration|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
*/
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setResampleStepDuration = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.clearResampleStepDuration = function() {
  return this.setResampleStepDuration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.hasResampleStepDuration = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional int32 page_size = 5;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional string page_token = 6;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional float low_pass_filter_cutoff_hz = 7;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.getLowPassFilterCutoffHz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesRequest.prototype.setLowPassFilterCutoffHz = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    sampleGroupsList: jspb.Message.toObjectList(msg.getSampleGroupsList(),
    proto.tytorobotics.flightstand.v1.SampleGroup.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, ""),
    progress: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0),
    rowsCount: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListTestSamplesResponse;
  return proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.SampleGroup;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinaryFromReader);
      msg.addSampleGroups(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setProgress(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readUint64());
      msg.setRowsCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSampleGroupsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.SampleGroup.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getProgress();
  if (f !== 0.0) {
    writer.writeFloat(
      3,
      f
    );
  }
  f = message.getRowsCount();
  if (f !== 0) {
    writer.writeUint64(
      4,
      f
    );
  }
};


/**
 * repeated SampleGroup sample_groups = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.getSampleGroupsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.SampleGroup, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.setSampleGroupsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.SampleGroup=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.addSampleGroups = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.SampleGroup, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.clearSampleGroupsList = function() {
  return this.setSampleGroupsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional float progress = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.getProgress = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.setProgress = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * optional uint64 rows_count = 4;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.getRowsCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestSamplesResponse.prototype.setRowsCount = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.SampleGroup.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SampleGroup.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SampleGroup} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SampleGroup.toObject = function(includeInstance, msg) {
  var f, obj = {
    groupTime: (f = msg.getGroupTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    samplesList: jspb.Message.toObjectList(msg.getSamplesList(),
    proto.tytorobotics.flightstand.v1.Sample.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SampleGroup;
  return proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SampleGroup} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setGroupTime(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.Sample;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Sample.deserializeBinaryFromReader);
      msg.addSamples(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SampleGroup.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SampleGroup} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SampleGroup.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getGroupTime();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getSamplesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.Sample.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.Timestamp group_time = 1;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.getGroupTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 1));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup} returns this
*/
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.setGroupTime = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup} returns this
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.clearGroupTime = function() {
  return this.setGroupTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.hasGroupTime = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated Sample samples = 2;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Sample>}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.getSamplesList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Sample>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Sample, 2));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Sample>} value
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup} returns this
*/
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.setSamplesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Sample=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Sample}
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.addSamples = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.tytorobotics.flightstand.v1.Sample, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup} returns this
 */
proto.tytorobotics.flightstand.v1.SampleGroup.prototype.clearSamplesList = function() {
  return this.setSamplesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.Test.repeatedFields_ = [7,9,10,11,12,13];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Test.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Test} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Test.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    createTime: (f = msg.getCreateTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    startTime: (f = msg.getStartTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    endTime: (f = msg.getEndTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    title: jspb.Message.getFieldWithDefault(msg, 5, ""),
    description: jspb.Message.getFieldWithDefault(msg, 6, ""),
    continuousSegmentsList: jspb.Message.toObjectList(msg.getContinuousSegmentsList(),
    proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.toObject, includeInstance),
    manualSamplesCount: jspb.Message.getFieldWithDefault(msg, 8, 0),
    boardsList: jspb.Message.toObjectList(msg.getBoardsList(),
    proto.tytorobotics.flightstand.v1.Board.toObject, includeInstance),
    inputsList: jspb.Message.toObjectList(msg.getInputsList(),
    proto.tytorobotics.flightstand.v1.Input.toObject, includeInstance),
    outputsList: jspb.Message.toObjectList(msg.getOutputsList(),
    proto.tytorobotics.flightstand.v1.Output.toObject, includeInstance),
    powertrainsList: jspb.Message.toObjectList(msg.getPowertrainsList(),
    proto.tytorobotics.flightstand.v1.Powertrain.toObject, includeInstance),
    devBoardParametersList: jspb.Message.toObjectList(msg.getDevBoardParametersList(),
    proto.tytorobotics.flightstand.v1.BoardParameter.toObject, includeInstance),
    cutoffCondition: (f = msg.getCutoffCondition()) && proto.tytorobotics.flightstand.v1.CutoffCondition.toObject(includeInstance, f),
    lowPassFilterCutoffHz: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0),
    localFilePath: jspb.Message.getFieldWithDefault(msg, 16, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Test}
 */
proto.tytorobotics.flightstand.v1.Test.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Test;
  return proto.tytorobotics.flightstand.v1.Test.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Test} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Test}
 */
proto.tytorobotics.flightstand.v1.Test.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreateTime(value);
      break;
    case 3:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setStartTime(value);
      break;
    case 4:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setEndTime(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setTitle(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 7:
      var value = new proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinaryFromReader);
      msg.addContinuousSegments(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setManualSamplesCount(value);
      break;
    case 9:
      var value = new proto.tytorobotics.flightstand.v1.Board;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Board.deserializeBinaryFromReader);
      msg.addBoards(value);
      break;
    case 10:
      var value = new proto.tytorobotics.flightstand.v1.Input;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Input.deserializeBinaryFromReader);
      msg.addInputs(value);
      break;
    case 11:
      var value = new proto.tytorobotics.flightstand.v1.Output;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Output.deserializeBinaryFromReader);
      msg.addOutputs(value);
      break;
    case 12:
      var value = new proto.tytorobotics.flightstand.v1.Powertrain;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinaryFromReader);
      msg.addPowertrains(value);
      break;
    case 13:
      var value = new proto.tytorobotics.flightstand.v1.BoardParameter;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinaryFromReader);
      msg.addDevBoardParameters(value);
      break;
    case 14:
      var value = new proto.tytorobotics.flightstand.v1.CutoffCondition;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinaryFromReader);
      msg.setCutoffCondition(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLowPassFilterCutoffHz(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setLocalFilePath(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Test.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Test} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Test.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCreateTime();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getStartTime();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getEndTime();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getTitle();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getContinuousSegmentsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.serializeBinaryToWriter
    );
  }
  f = message.getManualSamplesCount();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getBoardsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      proto.tytorobotics.flightstand.v1.Board.serializeBinaryToWriter
    );
  }
  f = message.getInputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      10,
      f,
      proto.tytorobotics.flightstand.v1.Input.serializeBinaryToWriter
    );
  }
  f = message.getOutputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      11,
      f,
      proto.tytorobotics.flightstand.v1.Output.serializeBinaryToWriter
    );
  }
  f = message.getPowertrainsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      12,
      f,
      proto.tytorobotics.flightstand.v1.Powertrain.serializeBinaryToWriter
    );
  }
  f = message.getDevBoardParametersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      13,
      f,
      proto.tytorobotics.flightstand.v1.BoardParameter.serializeBinaryToWriter
    );
  }
  f = message.getCutoffCondition();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.tytorobotics.flightstand.v1.CutoffCondition.serializeBinaryToWriter
    );
  }
  f = message.getLowPassFilterCutoffHz();
  if (f !== 0.0) {
    writer.writeFloat(
      15,
      f
    );
  }
  f = message.getLocalFilePath();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional google.protobuf.Timestamp create_time = 2;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getCreateTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 2));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setCreateTime = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearCreateTime = function() {
  return this.setCreateTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.hasCreateTime = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.Timestamp start_time = 3;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getStartTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 3));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setStartTime = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearStartTime = function() {
  return this.setStartTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.hasStartTime = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.Timestamp end_time = 4;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getEndTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 4));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setEndTime = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearEndTime = function() {
  return this.setEndTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.hasEndTime = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional string title = 5;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getTitle = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setTitle = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string description = 6;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * repeated ContinuousRecordingSegment continuous_segments = 7;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getContinuousSegmentsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, 7));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setContinuousSegmentsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addContinuousSegments = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearContinuousSegmentsList = function() {
  return this.setContinuousSegmentsList([]);
};


/**
 * optional int32 manual_samples_count = 8;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getManualSamplesCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setManualSamplesCount = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * repeated Board boards = 9;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Board>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getBoardsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Board>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Board, 9));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Board>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setBoardsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Board=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Board}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addBoards = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.tytorobotics.flightstand.v1.Board, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearBoardsList = function() {
  return this.setBoardsList([]);
};


/**
 * repeated Input inputs = 10;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Input>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getInputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Input>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Input, 10));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Input>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setInputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 10, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Input=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Input}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addInputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 10, opt_value, proto.tytorobotics.flightstand.v1.Input, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearInputsList = function() {
  return this.setInputsList([]);
};


/**
 * repeated Output outputs = 11;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Output>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getOutputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Output>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Output, 11));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Output>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setOutputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 11, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Output=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Output}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addOutputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 11, opt_value, proto.tytorobotics.flightstand.v1.Output, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearOutputsList = function() {
  return this.setOutputsList([]);
};


/**
 * repeated Powertrain powertrains = 12;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Powertrain>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getPowertrainsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Powertrain>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Powertrain, 12));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Powertrain>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setPowertrainsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 12, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Powertrain=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addPowertrains = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 12, opt_value, proto.tytorobotics.flightstand.v1.Powertrain, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearPowertrainsList = function() {
  return this.setPowertrainsList([]);
};


/**
 * repeated BoardParameter dev_board_parameters = 13;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getDevBoardParametersList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.BoardParameter, 13));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setDevBoardParametersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 13, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.BoardParameter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.addDevBoardParameters = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 13, opt_value, proto.tytorobotics.flightstand.v1.BoardParameter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearDevBoardParametersList = function() {
  return this.setDevBoardParametersList([]);
};


/**
 * optional CutoffCondition cutoff_condition = 14;
 * @return {?proto.tytorobotics.flightstand.v1.CutoffCondition}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getCutoffCondition = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.CutoffCondition} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.CutoffCondition, 14));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.CutoffCondition|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
*/
proto.tytorobotics.flightstand.v1.Test.prototype.setCutoffCondition = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.clearCutoffCondition = function() {
  return this.setCutoffCondition(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.hasCutoffCondition = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional float low_pass_filter_cutoff_hz = 15;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getLowPassFilterCutoffHz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setLowPassFilterCutoffHz = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};


/**
 * optional string local_file_path = 16;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Test.prototype.getLocalFilePath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Test} returns this
 */
proto.tytorobotics.flightstand.v1.Test.prototype.setLocalFilePath = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.Core.repeatedFields_ = [4];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Core.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Core} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Core.toObject = function(includeInstance, msg) {
  var f, obj = {
    remoteToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    inputsTaring: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    cutoffActive: (f = msg.getCutoffActive()) && proto.tytorobotics.flightstand.v1.CutoffCondition.toObject(includeInstance, f),
    coreSignalsList: jspb.Message.toObjectList(msg.getCoreSignalsList(),
    proto.tytorobotics.flightstand.v1.CoreSignal.toObject, includeInstance),
    debug: jspb.Message.getBooleanFieldWithDefault(msg, 5, false),
    developer: jspb.Message.getBooleanFieldWithDefault(msg, 6, false),
    devPassword: jspb.Message.getFieldWithDefault(msg, 7, ""),
    devCutoffsDisabled: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    lowPassFilterCutoffHz: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    actionSequenceRunningUntil: (f = msg.getActionSequenceRunningUntil()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    licenseKey: jspb.Message.getFieldWithDefault(msg, 11, ""),
    licenseError: jspb.Message.getFieldWithDefault(msg, 12, ""),
    licenseExpiration: (f = msg.getLicenseExpiration()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    licenseOfflineUntil: (f = msg.getLicenseOfflineUntil()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    licenseFeatureExternalBoards: jspb.Message.getBooleanFieldWithDefault(msg, 15, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Core}
 */
proto.tytorobotics.flightstand.v1.Core.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Core;
  return proto.tytorobotics.flightstand.v1.Core.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Core} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Core}
 */
proto.tytorobotics.flightstand.v1.Core.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRemoteToken(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setInputsTaring(value);
      break;
    case 3:
      var value = new proto.tytorobotics.flightstand.v1.CutoffCondition;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinaryFromReader);
      msg.setCutoffActive(value);
      break;
    case 4:
      var value = new proto.tytorobotics.flightstand.v1.CoreSignal;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.CoreSignal.deserializeBinaryFromReader);
      msg.addCoreSignals(value);
      break;
    case 5:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDebug(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDeveloper(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevPassword(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDevCutoffsDisabled(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLowPassFilterCutoffHz(value);
      break;
    case 10:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setActionSequenceRunningUntil(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setLicenseKey(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setLicenseError(value);
      break;
    case 13:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setLicenseExpiration(value);
      break;
    case 14:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setLicenseOfflineUntil(value);
      break;
    case 15:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setLicenseFeatureExternalBoards(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Core.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Core} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Core.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRemoteToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getInputsTaring();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getCutoffActive();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.tytorobotics.flightstand.v1.CutoffCondition.serializeBinaryToWriter
    );
  }
  f = message.getCoreSignalsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      proto.tytorobotics.flightstand.v1.CoreSignal.serializeBinaryToWriter
    );
  }
  f = message.getDebug();
  if (f) {
    writer.writeBool(
      5,
      f
    );
  }
  f = message.getDeveloper();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
  f = message.getDevPassword();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getDevCutoffsDisabled();
  if (f) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getLowPassFilterCutoffHz();
  if (f !== 0.0) {
    writer.writeFloat(
      9,
      f
    );
  }
  f = message.getActionSequenceRunningUntil();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getLicenseKey();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getLicenseError();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getLicenseExpiration();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getLicenseOfflineUntil();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getLicenseFeatureExternalBoards();
  if (f) {
    writer.writeBool(
      15,
      f
    );
  }
};


/**
 * optional string remote_token = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getRemoteToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setRemoteToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool inputs_taring = 2;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getInputsTaring = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setInputsTaring = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional CutoffCondition cutoff_active = 3;
 * @return {?proto.tytorobotics.flightstand.v1.CutoffCondition}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getCutoffActive = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.CutoffCondition} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.CutoffCondition, 3));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.CutoffCondition|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
*/
proto.tytorobotics.flightstand.v1.Core.prototype.setCutoffActive = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.clearCutoffActive = function() {
  return this.setCutoffActive(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.hasCutoffActive = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * repeated CoreSignal core_signals = 4;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.CoreSignal>}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getCoreSignalsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.CoreSignal>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.CoreSignal, 4));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.CoreSignal>} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
*/
proto.tytorobotics.flightstand.v1.Core.prototype.setCoreSignalsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.CoreSignal=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignal}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.addCoreSignals = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.tytorobotics.flightstand.v1.CoreSignal, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.clearCoreSignalsList = function() {
  return this.setCoreSignalsList([]);
};


/**
 * optional bool debug = 5;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getDebug = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 5, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setDebug = function(value) {
  return jspb.Message.setProto3BooleanField(this, 5, value);
};


/**
 * optional bool developer = 6;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getDeveloper = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setDeveloper = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};


/**
 * optional string dev_password = 7;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getDevPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setDevPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional bool dev_cutoffs_disabled = 8;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getDevCutoffsDisabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setDevCutoffsDisabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 8, value);
};


/**
 * optional float low_pass_filter_cutoff_hz = 9;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLowPassFilterCutoffHz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setLowPassFilterCutoffHz = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional google.protobuf.Timestamp action_sequence_running_until = 10;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getActionSequenceRunningUntil = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 10));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
*/
proto.tytorobotics.flightstand.v1.Core.prototype.setActionSequenceRunningUntil = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.clearActionSequenceRunningUntil = function() {
  return this.setActionSequenceRunningUntil(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.hasActionSequenceRunningUntil = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional string license_key = 11;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLicenseKey = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setLicenseKey = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string license_error = 12;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLicenseError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setLicenseError = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional google.protobuf.Timestamp license_expiration = 13;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLicenseExpiration = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 13));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
*/
proto.tytorobotics.flightstand.v1.Core.prototype.setLicenseExpiration = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.clearLicenseExpiration = function() {
  return this.setLicenseExpiration(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.hasLicenseExpiration = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional google.protobuf.Timestamp license_offline_until = 14;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLicenseOfflineUntil = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 14));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
*/
proto.tytorobotics.flightstand.v1.Core.prototype.setLicenseOfflineUntil = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.clearLicenseOfflineUntil = function() {
  return this.setLicenseOfflineUntil(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.hasLicenseOfflineUntil = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional bool license_feature_external_boards = 15;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Core.prototype.getLicenseFeatureExternalBoards = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 15, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Core} returns this
 */
proto.tytorobotics.flightstand.v1.Core.prototype.setLicenseFeatureExternalBoards = function(value) {
  return jspb.Message.setProto3BooleanField(this, 15, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.Board.repeatedFields_ = [17];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Board.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Board} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Board.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    path: jspb.Message.getFieldWithDefault(msg, 2, ""),
    ready: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    connecting: jspb.Message.getBooleanFieldWithDefault(msg, 4, false),
    runtimeError: jspb.Message.getFieldWithDefault(msg, 5, ""),
    firmwareVersion: jspb.Message.getFieldWithDefault(msg, 6, ""),
    expectedFirmwareVersion: jspb.Message.getFieldWithDefault(msg, 7, ""),
    firmwareFlashing: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    firmwareFlashError: jspb.Message.getFieldWithDefault(msg, 9, ""),
    synchronisationError: jspb.Message.getFieldWithDefault(msg, 10, ""),
    displayName: jspb.Message.getFieldWithDefault(msg, 11, ""),
    modelNumber: jspb.Message.getFieldWithDefault(msg, 12, ""),
    serialNumber: jspb.Message.getFieldWithDefault(msg, 13, ""),
    connectionError: jspb.Message.getFieldWithDefault(msg, 14, ""),
    disconnected: jspb.Message.getBooleanFieldWithDefault(msg, 15, false),
    synchronizing: jspb.Message.getBooleanFieldWithDefault(msg, 16, false),
    faultCodesList: (f = jspb.Message.getRepeatedField(msg, 17)) == null ? undefined : f,
    pb_synchronized: jspb.Message.getBooleanFieldWithDefault(msg, 18, false),
    limitSwitchState: jspb.Message.getBooleanFieldWithDefault(msg, 19, false),
    busy: jspb.Message.getBooleanFieldWithDefault(msg, 20, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Board}
 */
proto.tytorobotics.flightstand.v1.Board.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Board;
  return proto.tytorobotics.flightstand.v1.Board.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Board} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Board}
 */
proto.tytorobotics.flightstand.v1.Board.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setReady(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setConnecting(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setRuntimeError(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirmwareVersion(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setExpectedFirmwareVersion(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setFirmwareFlashing(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirmwareFlashError(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setSynchronisationError(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setDisplayName(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setModelNumber(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setSerialNumber(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setConnectionError(value);
      break;
    case 15:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDisconnected(value);
      break;
    case 16:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSynchronizing(value);
      break;
    case 17:
      var values = /** @type {!Array<number>} */ (reader.isDelimited() ? reader.readPackedInt32() : [reader.readInt32()]);
      for (var i = 0; i < values.length; i++) {
        msg.addFaultCodes(values[i]);
      }
      break;
    case 18:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSynchronized(value);
      break;
    case 19:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setLimitSwitchState(value);
      break;
    case 20:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setBusy(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Board.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Board} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Board.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getReady();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getConnecting();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
  f = message.getRuntimeError();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getFirmwareVersion();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getExpectedFirmwareVersion();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getFirmwareFlashing();
  if (f) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getFirmwareFlashError();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getSynchronisationError();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getDisplayName();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getModelNumber();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getSerialNumber();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getConnectionError();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getDisconnected();
  if (f) {
    writer.writeBool(
      15,
      f
    );
  }
  f = message.getSynchronizing();
  if (f) {
    writer.writeBool(
      16,
      f
    );
  }
  f = message.getFaultCodesList();
  if (f.length > 0) {
    writer.writePackedInt32(
      17,
      f
    );
  }
  f = message.getSynchronized();
  if (f) {
    writer.writeBool(
      18,
      f
    );
  }
  f = message.getLimitSwitchState();
  if (f) {
    writer.writeBool(
      19,
      f
    );
  }
  f = message.getBusy();
  if (f) {
    writer.writeBool(
      20,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string path = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional bool ready = 3;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getReady = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setReady = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional bool connecting = 4;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getConnecting = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setConnecting = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};


/**
 * optional string runtime_error = 5;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getRuntimeError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setRuntimeError = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string firmware_version = 6;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getFirmwareVersion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setFirmwareVersion = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string expected_firmware_version = 7;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getExpectedFirmwareVersion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setExpectedFirmwareVersion = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional bool firmware_flashing = 8;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getFirmwareFlashing = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setFirmwareFlashing = function(value) {
  return jspb.Message.setProto3BooleanField(this, 8, value);
};


/**
 * optional string firmware_flash_error = 9;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getFirmwareFlashError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setFirmwareFlashError = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string synchronisation_error = 10;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getSynchronisationError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setSynchronisationError = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string display_name = 11;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string model_number = 12;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getModelNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setModelNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string serial_number = 13;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getSerialNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setSerialNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string connection_error = 14;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getConnectionError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setConnectionError = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional bool disconnected = 15;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getDisconnected = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 15, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setDisconnected = function(value) {
  return jspb.Message.setProto3BooleanField(this, 15, value);
};


/**
 * optional bool synchronizing = 16;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getSynchronizing = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 16, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setSynchronizing = function(value) {
  return jspb.Message.setProto3BooleanField(this, 16, value);
};


/**
 * repeated int32 fault_codes = 17;
 * @return {!Array<number>}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getFaultCodesList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 17));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setFaultCodesList = function(value) {
  return jspb.Message.setField(this, 17, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.addFaultCodes = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 17, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.clearFaultCodesList = function() {
  return this.setFaultCodesList([]);
};


/**
 * optional bool synchronized = 18;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getSynchronized = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 18, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setSynchronized = function(value) {
  return jspb.Message.setProto3BooleanField(this, 18, value);
};


/**
 * optional bool limit_switch_state = 19;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getLimitSwitchState = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 19, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setLimitSwitchState = function(value) {
  return jspb.Message.setProto3BooleanField(this, 19, value);
};


/**
 * optional bool busy = 20;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Board.prototype.getBusy = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 20, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Board} returns this
 */
proto.tytorobotics.flightstand.v1.Board.prototype.setBusy = function(value) {
  return jspb.Message.setProto3BooleanField(this, 20, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Input.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Input} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Input.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    closed: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    alias: jspb.Message.getFieldWithDefault(msg, 3, ""),
    displayName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    inputType: jspb.Message.getFieldWithDefault(msg, 5, 0),
    hasTare: jspb.Message.getBooleanFieldWithDefault(msg, 6, false),
    tareOffset: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0),
    debugOnly: jspb.Message.getBooleanFieldWithDefault(msg, 8, false),
    hasEnableSwitch: jspb.Message.getBooleanFieldWithDefault(msg, 9, false),
    enabled: jspb.Message.getBooleanFieldWithDefault(msg, 10, false),
    developerOnly: jspb.Message.getBooleanFieldWithDefault(msg, 11, false),
    hasReverse: jspb.Message.getBooleanFieldWithDefault(msg, 12, false),
    reversed: jspb.Message.getBooleanFieldWithDefault(msg, 13, false),
    hasIntegerDivider: jspb.Message.getBooleanFieldWithDefault(msg, 14, false),
    integerDivider: jspb.Message.getFieldWithDefault(msg, 15, 0),
    signalName: jspb.Message.getFieldWithDefault(msg, 16, ""),
    rawSignalName: jspb.Message.getFieldWithDefault(msg, 17, ""),
    minUserCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 18, 0.0),
    maxUserCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 19, 0.0),
    hasMinUserCutoff: jspb.Message.getBooleanFieldWithDefault(msg, 20, false),
    hasMaxUserCutoff: jspb.Message.getBooleanFieldWithDefault(msg, 21, false),
    minSystemCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 22, 0.0),
    maxSystemCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 23, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Input}
 */
proto.tytorobotics.flightstand.v1.Input.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Input;
  return proto.tytorobotics.flightstand.v1.Input.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Input} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Input}
 */
proto.tytorobotics.flightstand.v1.Input.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setClosed(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setAlias(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDisplayName(value);
      break;
    case 5:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.InputType} */ (reader.readEnum());
      msg.setInputType(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasTare(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setTareOffset(value);
      break;
    case 8:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDebugOnly(value);
      break;
    case 9:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasEnableSwitch(value);
      break;
    case 10:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnabled(value);
      break;
    case 11:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDeveloperOnly(value);
      break;
    case 12:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasReverse(value);
      break;
    case 13:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setReversed(value);
      break;
    case 14:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasIntegerDivider(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setIntegerDivider(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setRawSignalName(value);
      break;
    case 18:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinUserCutoff(value);
      break;
    case 19:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxUserCutoff(value);
      break;
    case 20:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasMinUserCutoff(value);
      break;
    case 21:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setHasMaxUserCutoff(value);
      break;
    case 22:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinSystemCutoff(value);
      break;
    case 23:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxSystemCutoff(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Input.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Input} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Input.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getClosed();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getAlias();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDisplayName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getInputType();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getHasTare();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
  f = message.getTareOffset();
  if (f !== 0.0) {
    writer.writeFloat(
      7,
      f
    );
  }
  f = message.getDebugOnly();
  if (f) {
    writer.writeBool(
      8,
      f
    );
  }
  f = message.getHasEnableSwitch();
  if (f) {
    writer.writeBool(
      9,
      f
    );
  }
  f = message.getEnabled();
  if (f) {
    writer.writeBool(
      10,
      f
    );
  }
  f = message.getDeveloperOnly();
  if (f) {
    writer.writeBool(
      11,
      f
    );
  }
  f = message.getHasReverse();
  if (f) {
    writer.writeBool(
      12,
      f
    );
  }
  f = message.getReversed();
  if (f) {
    writer.writeBool(
      13,
      f
    );
  }
  f = message.getHasIntegerDivider();
  if (f) {
    writer.writeBool(
      14,
      f
    );
  }
  f = message.getIntegerDivider();
  if (f !== 0) {
    writer.writeInt32(
      15,
      f
    );
  }
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getRawSignalName();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getMinUserCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      18,
      f
    );
  }
  f = message.getMaxUserCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      19,
      f
    );
  }
  f = message.getHasMinUserCutoff();
  if (f) {
    writer.writeBool(
      20,
      f
    );
  }
  f = message.getHasMaxUserCutoff();
  if (f) {
    writer.writeBool(
      21,
      f
    );
  }
  f = message.getMinSystemCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      22,
      f
    );
  }
  f = message.getMaxSystemCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      23,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool closed = 2;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getClosed = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setClosed = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional string alias = 3;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getAlias = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setAlias = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string display_name = 4;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional InputType input_type = 5;
 * @return {!proto.tytorobotics.flightstand.v1.InputType}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getInputType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.InputType} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.InputType} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setInputType = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * optional bool has_tare = 6;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasTare = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasTare = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};


/**
 * optional float tare_offset = 7;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getTareOffset = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setTareOffset = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};


/**
 * optional bool debug_only = 8;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getDebugOnly = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 8, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setDebugOnly = function(value) {
  return jspb.Message.setProto3BooleanField(this, 8, value);
};


/**
 * optional bool has_enable_switch = 9;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasEnableSwitch = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 9, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasEnableSwitch = function(value) {
  return jspb.Message.setProto3BooleanField(this, 9, value);
};


/**
 * optional bool enabled = 10;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 10, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 10, value);
};


/**
 * optional bool developer_only = 11;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getDeveloperOnly = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 11, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setDeveloperOnly = function(value) {
  return jspb.Message.setProto3BooleanField(this, 11, value);
};


/**
 * optional bool has_reverse = 12;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasReverse = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 12, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasReverse = function(value) {
  return jspb.Message.setProto3BooleanField(this, 12, value);
};


/**
 * optional bool reversed = 13;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getReversed = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 13, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setReversed = function(value) {
  return jspb.Message.setProto3BooleanField(this, 13, value);
};


/**
 * optional bool has_integer_divider = 14;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasIntegerDivider = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 14, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasIntegerDivider = function(value) {
  return jspb.Message.setProto3BooleanField(this, 14, value);
};


/**
 * optional int32 integer_divider = 15;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getIntegerDivider = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 15, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setIntegerDivider = function(value) {
  return jspb.Message.setProto3IntField(this, 15, value);
};


/**
 * optional string signal_name = 16;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string raw_signal_name = 17;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getRawSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setRawSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * optional float min_user_cutoff = 18;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getMinUserCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 18, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setMinUserCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 18, value);
};


/**
 * optional float max_user_cutoff = 19;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getMaxUserCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 19, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setMaxUserCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 19, value);
};


/**
 * optional bool has_min_user_cutoff = 20;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasMinUserCutoff = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 20, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasMinUserCutoff = function(value) {
  return jspb.Message.setProto3BooleanField(this, 20, value);
};


/**
 * optional bool has_max_user_cutoff = 21;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getHasMaxUserCutoff = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 21, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setHasMaxUserCutoff = function(value) {
  return jspb.Message.setProto3BooleanField(this, 21, value);
};


/**
 * optional float min_system_cutoff = 22;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getMinSystemCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 22, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setMinSystemCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 22, value);
};


/**
 * optional float max_system_cutoff = 23;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Input.prototype.getMaxSystemCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 23, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Input} returns this
 */
proto.tytorobotics.flightstand.v1.Input.prototype.setMaxSystemCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 23, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.OutputTarget.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.OutputTarget} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.OutputTarget.toObject = function(includeInstance, msg) {
  var f, obj = {
    active: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    targetValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    rateLimitPerSecond: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.OutputTarget}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.OutputTarget;
  return proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.OutputTarget} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.OutputTarget}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setActive(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setTargetValue(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setRateLimitPerSecond(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.OutputTarget.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.OutputTarget} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.OutputTarget.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getActive();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getTargetValue();
  if (f !== 0.0) {
    writer.writeFloat(
      2,
      f
    );
  }
  f = message.getRateLimitPerSecond();
  if (f !== 0.0) {
    writer.writeFloat(
      3,
      f
    );
  }
};


/**
 * optional bool active = 1;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.getActive = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.OutputTarget} returns this
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.setActive = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional float target_value = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.getTargetValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.OutputTarget} returns this
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.setTargetValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional float rate_limit_per_second = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.getRateLimitPerSecond = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.OutputTarget} returns this
 */
proto.tytorobotics.flightstand.v1.OutputTarget.prototype.setRateLimitPerSecond = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Signal.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Signal} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Signal.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    closed: jspb.Message.getBooleanFieldWithDefault(msg, 2, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Signal}
 */
proto.tytorobotics.flightstand.v1.Signal.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Signal;
  return proto.tytorobotics.flightstand.v1.Signal.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Signal} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Signal}
 */
proto.tytorobotics.flightstand.v1.Signal.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setClosed(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Signal.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Signal} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Signal.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getClosed();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Signal} returns this
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool closed = 2;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.getClosed = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Signal} returns this
 */
proto.tytorobotics.flightstand.v1.Signal.prototype.setClosed = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SignalStat.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SignalStat} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SignalStat.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    updateRateHz: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SignalStat}
 */
proto.tytorobotics.flightstand.v1.SignalStat.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SignalStat;
  return proto.tytorobotics.flightstand.v1.SignalStat.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SignalStat} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SignalStat}
 */
proto.tytorobotics.flightstand.v1.SignalStat.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setUpdateRateHz(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SignalStat.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SignalStat} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SignalStat.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getUpdateRateHz();
  if (f !== 0.0) {
    writer.writeFloat(
      2,
      f
    );
  }
};


/**
 * optional string signal_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.SignalStat} returns this
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional float update_rate_hz = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.getUpdateRateHz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.SignalStat} returns this
 */
proto.tytorobotics.flightstand.v1.SignalStat.prototype.setUpdateRateHz = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.Output.repeatedFields_ = [6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Output.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Output} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Output.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    closed: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    alias: jspb.Message.getFieldWithDefault(msg, 3, ""),
    displayName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    outputType: jspb.Message.getFieldWithDefault(msg, 5, 0),
    supportedProtocolsList: (f = jspb.Message.getRepeatedField(msg, 6)) == null ? undefined : f,
    cutoffTarget: (f = msg.getCutoffTarget()) && proto.tytorobotics.flightstand.v1.OutputTarget.toObject(includeInstance, f),
    protocol: jspb.Message.getFieldWithDefault(msg, 8, 0),
    outputTarget: (f = msg.getOutputTarget()) && proto.tytorobotics.flightstand.v1.OutputTarget.toObject(includeInstance, f),
    signalName: jspb.Message.getFieldWithDefault(msg, 10, ""),
    minSystemValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 11, 0.0),
    maxSystemValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0),
    minUserValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 13, 0.0),
    maxUserValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 14, 0.0),
    stepSize: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Output}
 */
proto.tytorobotics.flightstand.v1.Output.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Output;
  return proto.tytorobotics.flightstand.v1.Output.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Output} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Output}
 */
proto.tytorobotics.flightstand.v1.Output.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setClosed(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setAlias(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDisplayName(value);
      break;
    case 5:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.OutputType} */ (reader.readEnum());
      msg.setOutputType(value);
      break;
    case 6:
      var values = /** @type {!Array<!proto.tytorobotics.flightstand.v1.OutputProtocolType>} */ (reader.isDelimited() ? reader.readPackedEnum() : [reader.readEnum()]);
      for (var i = 0; i < values.length; i++) {
        msg.addSupportedProtocols(values[i]);
      }
      break;
    case 7:
      var value = new proto.tytorobotics.flightstand.v1.OutputTarget;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinaryFromReader);
      msg.setCutoffTarget(value);
      break;
    case 8:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.OutputProtocolType} */ (reader.readEnum());
      msg.setProtocol(value);
      break;
    case 9:
      var value = new proto.tytorobotics.flightstand.v1.OutputTarget;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinaryFromReader);
      msg.setOutputTarget(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinSystemValue(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxSystemValue(value);
      break;
    case 13:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinUserValue(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxUserValue(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setStepSize(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Output.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Output} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Output.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getClosed();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getAlias();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDisplayName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getOutputType();
  if (f !== 0.0) {
    writer.writeEnum(
      5,
      f
    );
  }
  f = message.getSupportedProtocolsList();
  if (f.length > 0) {
    writer.writePackedEnum(
      6,
      f
    );
  }
  f = message.getCutoffTarget();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.tytorobotics.flightstand.v1.OutputTarget.serializeBinaryToWriter
    );
  }
  f = message.getProtocol();
  if (f !== 0.0) {
    writer.writeEnum(
      8,
      f
    );
  }
  f = message.getOutputTarget();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.tytorobotics.flightstand.v1.OutputTarget.serializeBinaryToWriter
    );
  }
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getMinSystemValue();
  if (f !== 0.0) {
    writer.writeFloat(
      11,
      f
    );
  }
  f = message.getMaxSystemValue();
  if (f !== 0.0) {
    writer.writeFloat(
      12,
      f
    );
  }
  f = message.getMinUserValue();
  if (f !== 0.0) {
    writer.writeFloat(
      13,
      f
    );
  }
  f = message.getMaxUserValue();
  if (f !== 0.0) {
    writer.writeFloat(
      14,
      f
    );
  }
  f = message.getStepSize();
  if (f !== 0.0) {
    writer.writeFloat(
      15,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool closed = 2;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getClosed = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setClosed = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional string alias = 3;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getAlias = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setAlias = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string display_name = 4;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional OutputType output_type = 5;
 * @return {!proto.tytorobotics.flightstand.v1.OutputType}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getOutputType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.OutputType} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.OutputType} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setOutputType = function(value) {
  return jspb.Message.setProto3EnumField(this, 5, value);
};


/**
 * repeated OutputProtocolType supported_protocols = 6;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.OutputProtocolType>}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getSupportedProtocolsList = function() {
  return /** @type {!Array<!proto.tytorobotics.flightstand.v1.OutputProtocolType>} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.OutputProtocolType>} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setSupportedProtocolsList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.OutputProtocolType} value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.addSupportedProtocols = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.clearSupportedProtocolsList = function() {
  return this.setSupportedProtocolsList([]);
};


/**
 * optional OutputTarget cutoff_target = 7;
 * @return {?proto.tytorobotics.flightstand.v1.OutputTarget}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getCutoffTarget = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.OutputTarget} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.OutputTarget, 7));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.OutputTarget|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
*/
proto.tytorobotics.flightstand.v1.Output.prototype.setCutoffTarget = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.clearCutoffTarget = function() {
  return this.setCutoffTarget(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.hasCutoffTarget = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional OutputProtocolType protocol = 8;
 * @return {!proto.tytorobotics.flightstand.v1.OutputProtocolType}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getProtocol = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.OutputProtocolType} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.OutputProtocolType} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setProtocol = function(value) {
  return jspb.Message.setProto3EnumField(this, 8, value);
};


/**
 * optional OutputTarget output_target = 9;
 * @return {?proto.tytorobotics.flightstand.v1.OutputTarget}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getOutputTarget = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.OutputTarget} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.OutputTarget, 9));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.OutputTarget|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
*/
proto.tytorobotics.flightstand.v1.Output.prototype.setOutputTarget = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.clearOutputTarget = function() {
  return this.setOutputTarget(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.hasOutputTarget = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional string signal_name = 10;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional float min_system_value = 11;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getMinSystemValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 11, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setMinSystemValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 11, value);
};


/**
 * optional float max_system_value = 12;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getMaxSystemValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setMaxSystemValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};


/**
 * optional float min_user_value = 13;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getMinUserValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 13, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setMinUserValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 13, value);
};


/**
 * optional float max_user_value = 14;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getMaxUserValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 14, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setMaxUserValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 14, value);
};


/**
 * optional float step_size = 15;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Output.prototype.getStepSize = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Output} returns this
 */
proto.tytorobotics.flightstand.v1.Output.prototype.setStepSize = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.Powertrain.repeatedFields_ = [3,9,14];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Powertrain.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Powertrain} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Powertrain.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    throttleOutputName: jspb.Message.getFieldWithDefault(msg, 2, ""),
    extraOutputNamesList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    thrustInputName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    torqueInputName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    rotationSpeedInputName: jspb.Message.getFieldWithDefault(msg, 6, ""),
    voltageInputName: jspb.Message.getFieldWithDefault(msg, 7, ""),
    currentInputName: jspb.Message.getFieldWithDefault(msg, 8, ""),
    extraInputNamesList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    motorDisplayName: jspb.Message.getFieldWithDefault(msg, 10, ""),
    propellerDisplayName: jspb.Message.getFieldWithDefault(msg, 11, ""),
    escDisplayName: jspb.Message.getFieldWithDefault(msg, 12, ""),
    powerSourceDisplayName: jspb.Message.getFieldWithDefault(msg, 13, ""),
    powertrainSignalsList: jspb.Message.toObjectList(msg.getPowertrainSignalsList(),
    proto.tytorobotics.flightstand.v1.PowertrainSignal.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain}
 */
proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Powertrain;
  return proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Powertrain} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain}
 */
proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setThrottleOutputName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.addExtraOutputNames(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setThrustInputName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setTorqueInputName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setRotationSpeedInputName(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setVoltageInputName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setCurrentInputName(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.addExtraInputNames(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setMotorDisplayName(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setPropellerDisplayName(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setEscDisplayName(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setPowerSourceDisplayName(value);
      break;
    case 14:
      var value = new proto.tytorobotics.flightstand.v1.PowertrainSignal;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.PowertrainSignal.deserializeBinaryFromReader);
      msg.addPowertrainSignals(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Powertrain.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Powertrain} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Powertrain.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getThrottleOutputName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getExtraOutputNamesList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      3,
      f
    );
  }
  f = message.getThrustInputName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getTorqueInputName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getRotationSpeedInputName();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getVoltageInputName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getCurrentInputName();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getExtraInputNamesList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      9,
      f
    );
  }
  f = message.getMotorDisplayName();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getPropellerDisplayName();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getEscDisplayName();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getPowerSourceDisplayName();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getPowertrainSignalsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      14,
      f,
      proto.tytorobotics.flightstand.v1.PowertrainSignal.serializeBinaryToWriter
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string throttle_output_name = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getThrottleOutputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setThrottleOutputName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated string extra_output_names = 3;
 * @return {!Array<string>}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getExtraOutputNamesList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setExtraOutputNamesList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.addExtraOutputNames = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.clearExtraOutputNamesList = function() {
  return this.setExtraOutputNamesList([]);
};


/**
 * optional string thrust_input_name = 4;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getThrustInputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setThrustInputName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string torque_input_name = 5;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getTorqueInputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setTorqueInputName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string rotation_speed_input_name = 6;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getRotationSpeedInputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setRotationSpeedInputName = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string voltage_input_name = 7;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getVoltageInputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setVoltageInputName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string current_input_name = 8;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getCurrentInputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setCurrentInputName = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * repeated string extra_input_names = 9;
 * @return {!Array<string>}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getExtraInputNamesList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setExtraInputNamesList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.addExtraInputNames = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.clearExtraInputNamesList = function() {
  return this.setExtraInputNamesList([]);
};


/**
 * optional string motor_display_name = 10;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getMotorDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setMotorDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string propeller_display_name = 11;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getPropellerDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setPropellerDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string esc_display_name = 12;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getEscDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setEscDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string power_source_display_name = 13;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getPowerSourceDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setPowerSourceDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * repeated PowertrainSignal powertrain_signals = 14;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.PowertrainSignal>}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.getPowertrainSignalsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.PowertrainSignal>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.PowertrainSignal, 14));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.PowertrainSignal>} value
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
*/
proto.tytorobotics.flightstand.v1.Powertrain.prototype.setPowertrainSignalsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 14, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.PowertrainSignal=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignal}
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.addPowertrainSignals = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 14, opt_value, proto.tytorobotics.flightstand.v1.PowertrainSignal, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain} returns this
 */
proto.tytorobotics.flightstand.v1.Powertrain.prototype.clearPowertrainSignalsList = function() {
  return this.setPowertrainSignalsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.CutoffCondition.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.CutoffCondition} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.toObject = function(includeInstance, msg) {
  var f, obj = {
    type: jspb.Message.getFieldWithDefault(msg, 1, 0),
    cutoffTriggerSample: (f = msg.getCutoffTriggerSample()) && proto.tytorobotics.flightstand.v1.Sample.toObject(includeInstance, f),
    cutoffTriggerSignal: (f = msg.getCutoffTriggerSignal()) && proto.tytorobotics.flightstand.v1.Signal.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.CutoffCondition;
  return proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.CutoffCondition} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.CutoffConditionType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.Sample;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Sample.deserializeBinaryFromReader);
      msg.setCutoffTriggerSample(value);
      break;
    case 3:
      var value = new proto.tytorobotics.flightstand.v1.Signal;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Signal.deserializeBinaryFromReader);
      msg.setCutoffTriggerSignal(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.CutoffCondition.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.CutoffCondition} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getCutoffTriggerSample();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.Sample.serializeBinaryToWriter
    );
  }
  f = message.getCutoffTriggerSignal();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.tytorobotics.flightstand.v1.Signal.serializeBinaryToWriter
    );
  }
};


/**
 * optional CutoffConditionType type = 1;
 * @return {!proto.tytorobotics.flightstand.v1.CutoffConditionType}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.getType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.CutoffConditionType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.CutoffConditionType} value
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition} returns this
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional Sample cutoff_trigger_sample = 2;
 * @return {?proto.tytorobotics.flightstand.v1.Sample}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.getCutoffTriggerSample = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Sample} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Sample, 2));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Sample|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition} returns this
*/
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.setCutoffTriggerSample = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition} returns this
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.clearCutoffTriggerSample = function() {
  return this.setCutoffTriggerSample(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.hasCutoffTriggerSample = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional Signal cutoff_trigger_signal = 3;
 * @return {?proto.tytorobotics.flightstand.v1.Signal}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.getCutoffTriggerSignal = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Signal} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Signal, 3));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Signal|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition} returns this
*/
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.setCutoffTriggerSignal = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition} returns this
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.clearCutoffTriggerSignal = function() {
  return this.setCutoffTriggerSignal(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.CutoffCondition.prototype.hasCutoffTriggerSignal = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TimedActionSequence.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TimedActionSequence} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.toObject = function(includeInstance, msg) {
  var f, obj = {
    sequenceList: jspb.Message.toObjectList(msg.getSequenceList(),
    proto.tytorobotics.flightstand.v1.TimedAction.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TimedActionSequence}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TimedActionSequence;
  return proto.tytorobotics.flightstand.v1.TimedActionSequence.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TimedActionSequence} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TimedActionSequence}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.TimedAction;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.TimedAction.deserializeBinaryFromReader);
      msg.addSequence(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TimedActionSequence.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TimedActionSequence} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSequenceList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.TimedAction.serializeBinaryToWriter
    );
  }
};


/**
 * repeated TimedAction sequence = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.TimedAction>}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.getSequenceList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.TimedAction>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.TimedAction, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.TimedAction>} value
 * @return {!proto.tytorobotics.flightstand.v1.TimedActionSequence} returns this
*/
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.setSequenceList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.TimedAction=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction}
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.addSequence = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.TimedAction, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.TimedActionSequence} returns this
 */
proto.tytorobotics.flightstand.v1.TimedActionSequence.prototype.clearSequenceList = function() {
  return this.setSequenceList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.TimedAction.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TimedAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TimedAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TimedAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    actionTime: (f = msg.getActionTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    outputTargetActionsList: jspb.Message.toObjectList(msg.getOutputTargetActionsList(),
    proto.tytorobotics.flightstand.v1.OutputTargetAction.toObject, includeInstance),
    takeSample: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    triggerCutoff: jspb.Message.getBooleanFieldWithDefault(msg, 4, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction}
 */
proto.tytorobotics.flightstand.v1.TimedAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TimedAction;
  return proto.tytorobotics.flightstand.v1.TimedAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TimedAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction}
 */
proto.tytorobotics.flightstand.v1.TimedAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setActionTime(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.OutputTargetAction;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.OutputTargetAction.deserializeBinaryFromReader);
      msg.addOutputTargetActions(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setTakeSample(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setTriggerCutoff(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TimedAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TimedAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TimedAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getActionTime();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getOutputTargetActionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.OutputTargetAction.serializeBinaryToWriter
    );
  }
  f = message.getTakeSample();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getTriggerCutoff();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
};


/**
 * optional google.protobuf.Timestamp action_time = 1;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.getActionTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 1));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
*/
proto.tytorobotics.flightstand.v1.TimedAction.prototype.setActionTime = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.clearActionTime = function() {
  return this.setActionTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.hasActionTime = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated OutputTargetAction output_target_actions = 2;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.OutputTargetAction>}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.getOutputTargetActionsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.OutputTargetAction>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.OutputTargetAction, 2));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.OutputTargetAction>} value
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
*/
proto.tytorobotics.flightstand.v1.TimedAction.prototype.setOutputTargetActionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.OutputTargetAction=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.addOutputTargetActions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.tytorobotics.flightstand.v1.OutputTargetAction, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.clearOutputTargetActionsList = function() {
  return this.setOutputTargetActionsList([]);
};


/**
 * optional bool take_sample = 3;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.getTakeSample = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.setTakeSample = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional bool trigger_cutoff = 4;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.getTriggerCutoff = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.TimedAction} returns this
 */
proto.tytorobotics.flightstand.v1.TimedAction.prototype.setTriggerCutoff = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.OutputTargetAction.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.OutputTargetAction} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    outputTarget: (f = msg.getOutputTarget()) && proto.tytorobotics.flightstand.v1.OutputTarget.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.OutputTargetAction;
  return proto.tytorobotics.flightstand.v1.OutputTargetAction.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.OutputTargetAction} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputName(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.OutputTarget;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.OutputTarget.deserializeBinaryFromReader);
      msg.setOutputTarget(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.OutputTargetAction.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.OutputTargetAction} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOutputName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getOutputTarget();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.OutputTarget.serializeBinaryToWriter
    );
  }
};


/**
 * optional string output_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.getOutputName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction} returns this
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.setOutputName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional OutputTarget output_target = 2;
 * @return {?proto.tytorobotics.flightstand.v1.OutputTarget}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.getOutputTarget = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.OutputTarget} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.OutputTarget, 2));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.OutputTarget|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction} returns this
*/
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.setOutputTarget = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.OutputTargetAction} returns this
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.clearOutputTarget = function() {
  return this.setOutputTarget(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.OutputTargetAction.prototype.hasOutputTarget = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.BoardParameter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.BoardParameter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.BoardParameter.toObject = function(includeInstance, msg) {
  var f, obj = {
    parameterId: jspb.Message.getFieldWithDefault(msg, 1, 0),
    data: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.BoardParameter;
  return proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.BoardParameter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setParameterId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setData(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.BoardParameter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.BoardParameter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.BoardParameter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getParameterId();
  if (f !== 0) {
    writer.writeUint32(
      1,
      f
    );
  }
  f = message.getData();
  if (f !== 0) {
    writer.writeUint32(
      2,
      f
    );
  }
};


/**
 * optional uint32 parameter_id = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.getParameterId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter} returns this
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.setParameterId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional uint32 data = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.getData = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter} returns this
 */
proto.tytorobotics.flightstand.v1.BoardParameter.prototype.setData = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.PowertrainSignal.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.PowertrainSignal} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    type: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignal}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.PowertrainSignal;
  return proto.tytorobotics.flightstand.v1.PowertrainSignal.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.PowertrainSignal} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignal}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 2:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.PowertrainSignalType} */ (reader.readEnum());
      msg.setType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.PowertrainSignal.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.PowertrainSignal} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * optional string signal_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignal} returns this
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional PowertrainSignalType type = 2;
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignalType}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.getType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.PowertrainSignalType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.PowertrainSignalType} value
 * @return {!proto.tytorobotics.flightstand.v1.PowertrainSignal} returns this
 */
proto.tytorobotics.flightstand.v1.PowertrainSignal.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.CoreSignal.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.CoreSignal} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CoreSignal.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    type: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignal}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.CoreSignal;
  return proto.tytorobotics.flightstand.v1.CoreSignal.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.CoreSignal} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignal}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 2:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.CoreSignalType} */ (reader.readEnum());
      msg.setType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.CoreSignal.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.CoreSignal} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CoreSignal.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
};


/**
 * optional string signal_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignal} returns this
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional CoreSignalType type = 2;
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignalType}
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.getType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.CoreSignalType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.CoreSignalType} value
 * @return {!proto.tytorobotics.flightstand.v1.CoreSignal} returns this
 */
proto.tytorobotics.flightstand.v1.CoreSignal.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.Sample.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.Sample} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Sample.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    sampleTime: (f = msg.getSampleTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    active: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    value: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0),
    filteredValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 5, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.Sample}
 */
proto.tytorobotics.flightstand.v1.Sample.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.Sample;
  return proto.tytorobotics.flightstand.v1.Sample.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.Sample} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.Sample}
 */
proto.tytorobotics.flightstand.v1.Sample.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    case 2:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setSampleTime(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setActive(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setValue(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setFilteredValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.Sample.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.Sample} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.Sample.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSampleTime();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getActive();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeFloat(
      4,
      f
    );
  }
  f = message.getFilteredValue();
  if (f !== 0.0) {
    writer.writeFloat(
      5,
      f
    );
  }
};


/**
 * optional string signal_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional google.protobuf.Timestamp sample_time = 2;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.getSampleTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 2));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
*/
proto.tytorobotics.flightstand.v1.Sample.prototype.setSampleTime = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.clearSampleTime = function() {
  return this.setSampleTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.hasSampleTime = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional bool active = 3;
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.getActive = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.setActive = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional float value = 4;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.getValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.setValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};


/**
 * optional float filtered_value = 5;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.getFilteredValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 5, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.Sample} returns this
 */
proto.tytorobotics.flightstand.v1.Sample.prototype.setFilteredValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.toObject = function(includeInstance, msg) {
  var f, obj = {
    startTime: (f = msg.getStartTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    endTime: (f = msg.getEndTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    samplesCount: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment;
  return proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setStartTime(value);
      break;
    case 2:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setEndTime(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readUint64());
      msg.setSamplesCount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStartTime();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getEndTime();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getSamplesCount();
  if (f !== 0) {
    writer.writeUint64(
      3,
      f
    );
  }
};


/**
 * optional google.protobuf.Timestamp start_time = 1;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.getStartTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 1));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} returns this
*/
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.setStartTime = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} returns this
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.clearStartTime = function() {
  return this.setStartTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.hasStartTime = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.Timestamp end_time = 2;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.getEndTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 2));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} returns this
*/
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.setEndTime = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} returns this
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.clearEndTime = function() {
  return this.setEndTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.hasEndTime = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional uint64 samples_count = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.getSamplesCount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment} returns this
 */
proto.tytorobotics.flightstand.v1.ContinuousRecordingSegment.prototype.setSamplesCount = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ExternalInput.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ExternalInput} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ExternalInput.toObject = function(includeInstance, msg) {
  var f, obj = {
    inputType: jspb.Message.getFieldWithDefault(msg, 1, 0),
    minSystemCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    maxSystemCutoff: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ExternalInput;
  return proto.tytorobotics.flightstand.v1.ExternalInput.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ExternalInput} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.InputType} */ (reader.readEnum());
      msg.setInputType(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinSystemCutoff(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxSystemCutoff(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ExternalInput.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ExternalInput} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ExternalInput.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getInputType();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getMinSystemCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      2,
      f
    );
  }
  f = message.getMaxSystemCutoff();
  if (f !== 0.0) {
    writer.writeFloat(
      3,
      f
    );
  }
};


/**
 * optional InputType input_type = 1;
 * @return {!proto.tytorobotics.flightstand.v1.InputType}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.getInputType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.InputType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.InputType} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.setInputType = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional float min_system_cutoff = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.getMinSystemCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.setMinSystemCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional float max_system_cutoff = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.getMaxSystemCutoff = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalInput.prototype.setMaxSystemCutoff = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ExternalOutput.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ExternalOutput} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputType: jspb.Message.getFieldWithDefault(msg, 1, 0),
    minSystemValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    maxSystemValue: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0),
    stepSize: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ExternalOutput;
  return proto.tytorobotics.flightstand.v1.ExternalOutput.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ExternalOutput} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.tytorobotics.flightstand.v1.OutputType} */ (reader.readEnum());
      msg.setOutputType(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMinSystemValue(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setMaxSystemValue(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setStepSize(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ExternalOutput.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ExternalOutput} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOutputType();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getMinSystemValue();
  if (f !== 0.0) {
    writer.writeFloat(
      2,
      f
    );
  }
  f = message.getMaxSystemValue();
  if (f !== 0.0) {
    writer.writeFloat(
      3,
      f
    );
  }
  f = message.getStepSize();
  if (f !== 0.0) {
    writer.writeFloat(
      4,
      f
    );
  }
};


/**
 * optional OutputType output_type = 1;
 * @return {!proto.tytorobotics.flightstand.v1.OutputType}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.getOutputType = function() {
  return /** @type {!proto.tytorobotics.flightstand.v1.OutputType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.OutputType} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.setOutputType = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional float min_system_value = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.getMinSystemValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.setMinSystemValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional float max_system_value = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.getMaxSystemValue = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.setMaxSystemValue = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * optional float step_size = 4;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.getStepSize = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput} returns this
 */
proto.tytorobotics.flightstand.v1.ExternalOutput.prototype.setStepSize = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetServerStatusRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetServerStatusRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetServerStatusRequest}
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetServerStatusRequest;
  return proto.tytorobotics.flightstand.v1.GetServerStatusRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetServerStatusRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetServerStatusRequest}
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetServerStatusRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetServerStatusRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetServerStatusRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetCoreRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetCoreRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetCoreRequest}
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetCoreRequest;
  return proto.tytorobotics.flightstand.v1.GetCoreRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetCoreRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetCoreRequest}
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetCoreRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetCoreRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetCoreRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchCoreRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchCoreRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchCoreRequest}
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchCoreRequest;
  return proto.tytorobotics.flightstand.v1.WatchCoreRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchCoreRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchCoreRequest}
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchCoreRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchCoreRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchCoreRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TriggerCutoffRequest}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TriggerCutoffRequest;
  return proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TriggerCutoffRequest}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TriggerCutoffResponse}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TriggerCutoffResponse;
  return proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TriggerCutoffResponse}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TriggerCutoffResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TriggerCutoffResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearCutoffRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearCutoffRequest}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearCutoffRequest;
  return proto.tytorobotics.flightstand.v1.ClearCutoffRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearCutoffRequest}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearCutoffRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearCutoffRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearCutoffResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearCutoffResponse}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearCutoffResponse;
  return proto.tytorobotics.flightstand.v1.ClearCutoffResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearCutoffResponse}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearCutoffResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearCutoffResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearCutoffResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SyncTimestampsRequest}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SyncTimestampsRequest;
  return proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SyncTimestampsRequest}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SyncTimestampsResponse}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SyncTimestampsResponse;
  return proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SyncTimestampsResponse}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SyncTimestampsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SyncTimestampsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateCoreRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    core: (f = msg.getCore()) && proto.tytorobotics.flightstand.v1.Core.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateCoreRequest;
  return proto.tytorobotics.flightstand.v1.UpdateCoreRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Core;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Core.deserializeBinaryFromReader);
      msg.setCore(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateCoreRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCore();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Core.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional Core core = 1;
 * @return {?proto.tytorobotics.flightstand.v1.Core}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.getCore = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Core} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Core, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Core|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.setCore = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.clearCore = function() {
  return this.setCore(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.hasCore = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateCoreRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateCoreRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetBoardRequest}
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetBoardRequest;
  return proto.tytorobotics.flightstand.v1.GetBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetBoardRequest}
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetBoardRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListBoardsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsRequest}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListBoardsRequest;
  return proto.tytorobotics.flightstand.v1.ListBoardsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsRequest}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListBoardsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListBoardsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListBoardsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardsList: jspb.Message.toObjectList(msg.getBoardsList(),
    proto.tytorobotics.flightstand.v1.Board.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsResponse}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListBoardsResponse;
  return proto.tytorobotics.flightstand.v1.ListBoardsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsResponse}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Board;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Board.deserializeBinaryFromReader);
      msg.addBoards(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListBoardsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Board.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Board boards = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Board>}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.getBoardsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Board>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Board, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Board>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.setBoardsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Board=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Board}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.addBoards = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Board, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.clearBoardsList = function() {
  return this.setBoardsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListBoardsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListBoardsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchBoardsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchBoardsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchBoardsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchBoardsRequest;
  return proto.tytorobotics.flightstand.v1.WatchBoardsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchBoardsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchBoardsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchBoardsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchBoardsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchBoardsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DisconnectBoardRequest;
  return proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DisconnectBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DisconnectBoardResponse}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DisconnectBoardResponse;
  return proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DisconnectBoardResponse}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DisconnectBoardResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DisconnectBoardResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ResetBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ResetBoardRequest}
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ResetBoardRequest;
  return proto.tytorobotics.flightstand.v1.ResetBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ResetBoardRequest}
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ResetBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ResetBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ResetBoardRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ResetBoardResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ResetBoardResponse}
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ResetBoardResponse;
  return proto.tytorobotics.flightstand.v1.ResetBoardResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ResetBoardResponse}
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ResetBoardResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ResetBoardResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ResetBoardResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.IdentifyBoardRequest;
  return proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.IdentifyBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.IdentifyBoardResponse}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.IdentifyBoardResponse;
  return proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.IdentifyBoardResponse}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.IdentifyBoardResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.IdentifyBoardResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    devFilename: jspb.Message.getFieldWithDefault(msg, 2, ""),
    devFlashProtocol: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest;
  return proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevFilename(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDevFlashProtocol(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDevFilename();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDevFlashProtocol();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} returns this
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string dev_filename = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.getDevFilename = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} returns this
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.setDevFilename = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional int32 dev_flash_protocol = 3;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.getDevFlashProtocol = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest} returns this
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareRequest.prototype.setDevFlashProtocol = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse;
  return proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.FlashBoardFirmwareResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetInputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetInputRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetInputRequest}
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetInputRequest;
  return proto.tytorobotics.flightstand.v1.GetInputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetInputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetInputRequest}
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetInputRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetInputRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetInputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetInputRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateInputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    input: (f = msg.getInput()) && proto.tytorobotics.flightstand.v1.Input.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateInputRequest;
  return proto.tytorobotics.flightstand.v1.UpdateInputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Input;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Input.deserializeBinaryFromReader);
      msg.setInput(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateInputRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getInput();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Input.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional Input input = 1;
 * @return {?proto.tytorobotics.flightstand.v1.Input}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.getInput = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Input} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Input, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Input|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.setInput = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.clearInput = function() {
  return this.setInput(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.hasInput = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateInputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateInputRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListInputsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsRequest}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListInputsRequest;
  return proto.tytorobotics.flightstand.v1.ListInputsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsRequest}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListInputsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListInputsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListInputsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    inputsList: jspb.Message.toObjectList(msg.getInputsList(),
    proto.tytorobotics.flightstand.v1.Input.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsResponse}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListInputsResponse;
  return proto.tytorobotics.flightstand.v1.ListInputsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsResponse}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Input;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Input.deserializeBinaryFromReader);
      msg.addInputs(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListInputsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListInputsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getInputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Input.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Input inputs = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Input>}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.getInputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Input>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Input, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Input>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.setInputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Input=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Input}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.addInputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Input, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.clearInputsList = function() {
  return this.setInputsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListInputsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListInputsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchInputsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchInputsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchInputsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchInputsRequest;
  return proto.tytorobotics.flightstand.v1.WatchInputsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchInputsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchInputsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchInputsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchInputsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchInputsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TareInputsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TareInputsRequest}
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TareInputsRequest;
  return proto.tytorobotics.flightstand.v1.TareInputsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TareInputsRequest}
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TareInputsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TareInputsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TareInputsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TareInputsResponse}
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TareInputsResponse;
  return proto.tytorobotics.flightstand.v1.TareInputsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TareInputsResponse}
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TareInputsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TareInputsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TareInputsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSamplesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSamplesRequest;
  return proto.tytorobotics.flightstand.v1.ListSamplesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSamplesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListSamplesRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSamplesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    sampleGroup: (f = msg.getSampleGroup()) && proto.tytorobotics.flightstand.v1.SampleGroup.toObject(includeInstance, f),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSamplesResponse;
  return proto.tytorobotics.flightstand.v1.ListSamplesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.SampleGroup;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinaryFromReader);
      msg.setSampleGroup(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSamplesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSampleGroup();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.SampleGroup.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional SampleGroup sample_group = 1;
 * @return {?proto.tytorobotics.flightstand.v1.SampleGroup}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.getSampleGroup = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.SampleGroup} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.SampleGroup, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.SampleGroup|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.setSampleGroup = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.clearSampleGroup = function() {
  return this.setSampleGroup(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.hasSampleGroup = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListSamplesResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchSamplesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchSamplesRequest;
  return proto.tytorobotics.flightstand.v1.WatchSamplesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchSamplesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSamplesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchSamplesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    sampleGroupsList: jspb.Message.toObjectList(msg.getSampleGroupsList(),
    proto.tytorobotics.flightstand.v1.SampleGroup.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchSamplesResponse;
  return proto.tytorobotics.flightstand.v1.WatchSamplesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.SampleGroup;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.SampleGroup.deserializeBinaryFromReader);
      msg.addSampleGroups(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchSamplesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSampleGroupsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.SampleGroup.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SampleGroup sample_groups = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.getSampleGroupsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.SampleGroup, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.SampleGroup>} value
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse} returns this
*/
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.setSampleGroupsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.SampleGroup=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.SampleGroup}
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.addSampleGroups = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.SampleGroup, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSamplesResponse} returns this
 */
proto.tytorobotics.flightstand.v1.WatchSamplesResponse.prototype.clearSampleGroupsList = function() {
  return this.setSampleGroupsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    samplesList: jspb.Message.toObjectList(msg.getSamplesList(),
    proto.tytorobotics.flightstand.v1.Sample.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest;
  return proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Sample;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Sample.deserializeBinaryFromReader);
      msg.addSamples(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSamplesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Sample.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Sample samples = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Sample>}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.getSamplesList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Sample>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Sample, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Sample>} value
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest} returns this
*/
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.setSamplesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Sample=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Sample}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.addSamples = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Sample, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest} returns this
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesRequest.prototype.clearSamplesList = function() {
  return this.setSamplesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse;
  return proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WriteExternalSamplesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetSignalRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetSignalRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetSignalRequest}
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetSignalRequest;
  return proto.tytorobotics.flightstand.v1.GetSignalRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetSignalRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetSignalRequest}
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSignalName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetSignalRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetSignalRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string signal_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.prototype.getSignalName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetSignalRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetSignalRequest.prototype.setSignalName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSignalsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsRequest}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSignalsRequest;
  return proto.tytorobotics.flightstand.v1.ListSignalsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsRequest}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSignalsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListSignalsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSignalsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalsList: jspb.Message.toObjectList(msg.getSignalsList(),
    proto.tytorobotics.flightstand.v1.Signal.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsResponse}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSignalsResponse;
  return proto.tytorobotics.flightstand.v1.ListSignalsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsResponse}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Signal;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Signal.deserializeBinaryFromReader);
      msg.addSignals(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSignalsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Signal.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Signal signals = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Signal>}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.getSignalsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Signal>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Signal, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Signal>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.setSignalsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Signal=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Signal}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.addSignals = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Signal, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.clearSignalsList = function() {
  return this.setSignalsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListSignalsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchSignalsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchSignalsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSignalsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchSignalsRequest;
  return proto.tytorobotics.flightstand.v1.WatchSignalsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSignalsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchSignalsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchSignalsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchSignalsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchSignalsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetOutputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetOutputRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetOutputRequest}
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetOutputRequest;
  return proto.tytorobotics.flightstand.v1.GetOutputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetOutputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetOutputRequest}
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetOutputRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetOutputRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetOutputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetOutputRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateOutputRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    output: (f = msg.getOutput()) && proto.tytorobotics.flightstand.v1.Output.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateOutputRequest;
  return proto.tytorobotics.flightstand.v1.UpdateOutputRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Output;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Output.deserializeBinaryFromReader);
      msg.setOutput(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateOutputRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOutput();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Output.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional Output output = 1;
 * @return {?proto.tytorobotics.flightstand.v1.Output}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.getOutput = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Output} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Output, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Output|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.setOutput = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.clearOutput = function() {
  return this.setOutput(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.hasOutput = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateOutputRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateOutputRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListOutputsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsRequest}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListOutputsRequest;
  return proto.tytorobotics.flightstand.v1.ListOutputsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsRequest}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListOutputsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListOutputsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListOutputsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    outputsList: jspb.Message.toObjectList(msg.getOutputsList(),
    proto.tytorobotics.flightstand.v1.Output.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsResponse}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListOutputsResponse;
  return proto.tytorobotics.flightstand.v1.ListOutputsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsResponse}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Output;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Output.deserializeBinaryFromReader);
      msg.addOutputs(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListOutputsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getOutputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Output.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Output outputs = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Output>}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.getOutputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Output>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Output, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Output>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.setOutputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Output=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Output}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.addOutputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Output, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.clearOutputsList = function() {
  return this.setOutputsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListOutputsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListOutputsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchOutputsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchOutputsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchOutputsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchOutputsRequest;
  return proto.tytorobotics.flightstand.v1.WatchOutputsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchOutputsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchOutputsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchOutputsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchOutputsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchOutputsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest;
  return proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTimedActionSequenceRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    timedActionSequence: (f = msg.getTimedActionSequence()) && proto.tytorobotics.flightstand.v1.TimedActionSequence.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest;
  return proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.TimedActionSequence;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.TimedActionSequence.deserializeBinaryFromReader);
      msg.setTimedActionSequence(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTimedActionSequence();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.TimedActionSequence.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional TimedActionSequence timed_action_sequence = 1;
 * @return {?proto.tytorobotics.flightstand.v1.TimedActionSequence}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.getTimedActionSequence = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.TimedActionSequence} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.TimedActionSequence, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.TimedActionSequence|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.setTimedActionSequence = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.clearTimedActionSequence = function() {
  return this.setTimedActionSequence(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.hasTimedActionSequence = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest;
  return proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse;
  return proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest;
  return proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest}
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchTimedActionSequenceRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetPowertrainRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetPowertrainRequest;
  return proto.tytorobotics.flightstand.v1.GetPowertrainRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetPowertrainRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetPowertrainRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetPowertrainRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    powertrain: (f = msg.getPowertrain()) && proto.tytorobotics.flightstand.v1.Powertrain.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest;
  return proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Powertrain;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinaryFromReader);
      msg.setPowertrain(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPowertrain();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Powertrain.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional Powertrain powertrain = 1;
 * @return {?proto.tytorobotics.flightstand.v1.Powertrain}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.getPowertrain = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Powertrain} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Powertrain, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Powertrain|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.setPowertrain = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.clearPowertrain = function() {
  return this.setPowertrain(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.hasPowertrain = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdatePowertrainRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListPowertrainsRequest;
  return proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    powertrainsList: jspb.Message.toObjectList(msg.getPowertrainsList(),
    proto.tytorobotics.flightstand.v1.Powertrain.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListPowertrainsResponse;
  return proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Powertrain;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Powertrain.deserializeBinaryFromReader);
      msg.addPowertrains(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPowertrainsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Powertrain.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Powertrain powertrains = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Powertrain>}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.getPowertrainsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Powertrain>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Powertrain, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Powertrain>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.setPowertrainsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Powertrain=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Powertrain}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.addPowertrains = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Powertrain, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.clearPowertrainsList = function() {
  return this.setPowertrainsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListPowertrainsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListPowertrainsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearPowertrainRequest;
  return proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ClearPowertrainRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearPowertrainResponse}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearPowertrainResponse;
  return proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearPowertrainResponse}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearPowertrainResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearPowertrainResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest;
  return proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest}
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchPowertrainsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    pageSize: jspb.Message.getFieldWithDefault(msg, 2, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest;
  return proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int32 page_size = 2;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string page_token = 3;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardParametersList: jspb.Message.toObjectList(msg.getBoardParametersList(),
    proto.tytorobotics.flightstand.v1.BoardParameter.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse;
  return proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.BoardParameter;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinaryFromReader);
      msg.addBoardParameters(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardParametersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.BoardParameter.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated BoardParameter board_parameters = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.getBoardParametersList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.BoardParameter, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.BoardParameter>} value
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} returns this
*/
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.setBoardParametersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.BoardParameter=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.BoardParameter}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.addBoardParameters = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.BoardParameter, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} returns this
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.clearBoardParametersList = function() {
  return this.setBoardParametersList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse} returns this
 */
proto.tytorobotics.flightstand.v1.DevListBoardParametersResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, ""),
    boardParameter: (f = msg.getBoardParameter()) && proto.tytorobotics.flightstand.v1.BoardParameter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest;
  return proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.BoardParameter;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.BoardParameter.deserializeBinaryFromReader);
      msg.setBoardParameter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getBoardParameter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.BoardParameter.serializeBinaryToWriter
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional BoardParameter board_parameter = 2;
 * @return {?proto.tytorobotics.flightstand.v1.BoardParameter}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.getBoardParameter = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.BoardParameter} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.BoardParameter, 2));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.BoardParameter|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} returns this
*/
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.setBoardParameter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.clearBoardParameter = function() {
  return this.setBoardParameter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest.prototype.hasBoardParameter = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest;
  return proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse;
  return proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevClearBoardParametersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevBackupParametersRequest;
  return proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevBackupParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevBackupParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevBackupParametersResponse;
  return proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevBackupParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevBackupParametersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevBackupParametersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    boardName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest;
  return proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBoardName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBoardName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string board_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.prototype.getBoardName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersRequest.prototype.setBoardName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse;
  return proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DevRestoreParametersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest;
  return proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse;
  return proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateSimulatedBoardResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.repeatedFields_ = [1,2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    externalInputsList: jspb.Message.toObjectList(msg.getExternalInputsList(),
    proto.tytorobotics.flightstand.v1.ExternalInput.toObject, includeInstance),
    externalOutputsList: jspb.Message.toObjectList(msg.getExternalOutputsList(),
    proto.tytorobotics.flightstand.v1.ExternalOutput.toObject, includeInstance),
    displayName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    modelNumber: jspb.Message.getFieldWithDefault(msg, 4, ""),
    serialNumber: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest;
  return proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.ExternalInput;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.ExternalInput.deserializeBinaryFromReader);
      msg.addExternalInputs(value);
      break;
    case 2:
      var value = new proto.tytorobotics.flightstand.v1.ExternalOutput;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.ExternalOutput.deserializeBinaryFromReader);
      msg.addExternalOutputs(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDisplayName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setModelNumber(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setSerialNumber(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getExternalInputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.ExternalInput.serializeBinaryToWriter
    );
  }
  f = message.getExternalOutputsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      proto.tytorobotics.flightstand.v1.ExternalOutput.serializeBinaryToWriter
    );
  }
  f = message.getDisplayName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getModelNumber();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getSerialNumber();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * repeated ExternalInput external_inputs = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.ExternalInput>}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.getExternalInputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.ExternalInput>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.ExternalInput, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.ExternalInput>} value
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
*/
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.setExternalInputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.ExternalInput=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.ExternalInput}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.addExternalInputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.ExternalInput, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.clearExternalInputsList = function() {
  return this.setExternalInputsList([]);
};


/**
 * repeated ExternalOutput external_outputs = 2;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.ExternalOutput>}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.getExternalOutputsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.ExternalOutput>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.ExternalOutput, 2));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.ExternalOutput>} value
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
*/
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.setExternalOutputsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.ExternalOutput=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.ExternalOutput}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.addExternalOutputs = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.tytorobotics.flightstand.v1.ExternalOutput, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.clearExternalOutputsList = function() {
  return this.setExternalOutputsList([]);
};


/**
 * optional string display_name = 3;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.getDisplayName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.setDisplayName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string model_number = 4;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.getModelNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.setModelNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string serial_number = 5;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.getSerialNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest} returns this
 */
proto.tytorobotics.flightstand.v1.CreateExternalBoardRequest.prototype.setSerialNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    testRecorder: (f = msg.getTestRecorder()) && proto.tytorobotics.flightstand.v1.TestRecorder.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.TestRecorder;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.TestRecorder.deserializeBinaryFromReader);
      msg.setTestRecorder(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTestRecorder();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.TestRecorder.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional TestRecorder test_recorder = 1;
 * @return {?proto.tytorobotics.flightstand.v1.TestRecorder}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.getTestRecorder = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.TestRecorder} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.TestRecorder, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.TestRecorder|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.setTestRecorder = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.clearTestRecorder = function() {
  return this.setTestRecorder(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.hasTestRecorder = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRecorderRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.WatchTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse;
  return proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse;
  return proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ClearTestRecorderResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest;
  return proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    testName: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse;
  return proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setTestName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTestName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string test_name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.prototype.getTestName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse} returns this
 */
proto.tytorobotics.flightstand.v1.SaveTestRecorderResponse.prototype.setTestName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.GetTestRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.GetTestRequest}
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.GetTestRequest;
  return proto.tytorobotics.flightstand.v1.GetTestRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.GetTestRequest}
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.GetTestRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.GetTestRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.GetTestRequest} returns this
 */
proto.tytorobotics.flightstand.v1.GetTestRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.UpdateTestRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    test: (f = msg.getTest()) && proto.tytorobotics.flightstand.v1.Test.toObject(includeInstance, f),
    mask: (f = msg.getMask()) && google_protobuf_field_mask_pb.FieldMask.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.UpdateTestRequest;
  return proto.tytorobotics.flightstand.v1.UpdateTestRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Test;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Test.deserializeBinaryFromReader);
      msg.setTest(value);
      break;
    case 2:
      var value = new google_protobuf_field_mask_pb.FieldMask;
      reader.readMessage(value,google_protobuf_field_mask_pb.FieldMask.deserializeBinaryFromReader);
      msg.setMask(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.UpdateTestRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTest();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Test.serializeBinaryToWriter
    );
  }
  f = message.getMask();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_field_mask_pb.FieldMask.serializeBinaryToWriter
    );
  }
};


/**
 * optional Test test = 1;
 * @return {?proto.tytorobotics.flightstand.v1.Test}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.getTest = function() {
  return /** @type{?proto.tytorobotics.flightstand.v1.Test} */ (
    jspb.Message.getWrapperField(this, proto.tytorobotics.flightstand.v1.Test, 1));
};


/**
 * @param {?proto.tytorobotics.flightstand.v1.Test|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.setTest = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.clearTest = function() {
  return this.setTest(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.hasTest = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.FieldMask mask = 2;
 * @return {?proto.google.protobuf.FieldMask}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.getMask = function() {
  return /** @type{?proto.google.protobuf.FieldMask} */ (
    jspb.Message.getWrapperField(this, google_protobuf_field_mask_pb.FieldMask, 2));
};


/**
 * @param {?proto.google.protobuf.FieldMask|undefined} value
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} returns this
*/
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.setMask = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.tytorobotics.flightstand.v1.UpdateTestRequest} returns this
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.clearMask = function() {
  return this.setMask(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.tytorobotics.flightstand.v1.UpdateTestRequest.prototype.hasMask = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListTestsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pageSize: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsRequest}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListTestsRequest;
  return proto.tytorobotics.flightstand.v1.ListTestsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsRequest}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPageSize(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListTestsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPageSize();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 page_size = 1;
 * @return {number}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.getPageSize = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.setPageSize = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.getPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsRequest} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestsRequest.prototype.setPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListTestsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    testsList: jspb.Message.toObjectList(msg.getTestsList(),
    proto.tytorobotics.flightstand.v1.Test.toObject, includeInstance),
    nextPageToken: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsResponse}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListTestsResponse;
  return proto.tytorobotics.flightstand.v1.ListTestsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsResponse}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.Test;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.Test.deserializeBinaryFromReader);
      msg.addTests(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNextPageToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListTestsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListTestsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTestsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.Test.serializeBinaryToWriter
    );
  }
  f = message.getNextPageToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * repeated Test tests = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.Test>}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.getTestsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.Test>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.Test, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.Test>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.setTestsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.Test=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.Test}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.addTests = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.Test, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.clearTestsList = function() {
  return this.setTestsList([]);
};


/**
 * optional string next_page_token = 2;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.getNextPageToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.ListTestsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListTestsResponse.prototype.setNextPageToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.DeleteTestRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.DeleteTestRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.DeleteTestRequest}
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.DeleteTestRequest;
  return proto.tytorobotics.flightstand.v1.DeleteTestRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.DeleteTestRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.DeleteTestRequest}
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.DeleteTestRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.DeleteTestRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.tytorobotics.flightstand.v1.DeleteTestRequest} returns this
 */
proto.tytorobotics.flightstand.v1.DeleteTestRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest;
  return proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    cutoffConditionsList: jspb.Message.toObjectList(msg.getCutoffConditionsList(),
    proto.tytorobotics.flightstand.v1.CutoffCondition.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse;
  return proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.CutoffCondition;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.CutoffCondition.deserializeBinaryFromReader);
      msg.addCutoffConditions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCutoffConditionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.CutoffCondition.serializeBinaryToWriter
    );
  }
};


/**
 * repeated CutoffCondition cutoff_conditions = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.CutoffCondition>}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.getCutoffConditionsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.CutoffCondition>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.CutoffCondition, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.CutoffCondition>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.setCutoffConditionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.CutoffCondition=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.CutoffCondition}
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.addCutoffConditions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.CutoffCondition, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListCutoffConditionsResponse.prototype.clearCutoffConditionsList = function() {
  return this.setCutoffConditionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsRequest}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSignalStatsRequest;
  return proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsRequest}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    signalStatsList: jspb.Message.toObjectList(msg.getSignalStatsList(),
    proto.tytorobotics.flightstand.v1.SignalStat.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.tytorobotics.flightstand.v1.ListSignalStatsResponse;
  return proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.tytorobotics.flightstand.v1.SignalStat;
      reader.readMessage(value,proto.tytorobotics.flightstand.v1.SignalStat.deserializeBinaryFromReader);
      msg.addSignalStats(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSignalStatsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.tytorobotics.flightstand.v1.SignalStat.serializeBinaryToWriter
    );
  }
};


/**
 * repeated SignalStat signal_stats = 1;
 * @return {!Array<!proto.tytorobotics.flightstand.v1.SignalStat>}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.getSignalStatsList = function() {
  return /** @type{!Array<!proto.tytorobotics.flightstand.v1.SignalStat>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.tytorobotics.flightstand.v1.SignalStat, 1));
};


/**
 * @param {!Array<!proto.tytorobotics.flightstand.v1.SignalStat>} value
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse} returns this
*/
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.setSignalStatsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.tytorobotics.flightstand.v1.SignalStat=} opt_value
 * @param {number=} opt_index
 * @return {!proto.tytorobotics.flightstand.v1.SignalStat}
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.addSignalStats = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.tytorobotics.flightstand.v1.SignalStat, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.tytorobotics.flightstand.v1.ListSignalStatsResponse} returns this
 */
proto.tytorobotics.flightstand.v1.ListSignalStatsResponse.prototype.clearSignalStatsList = function() {
  return this.setSignalStatsList([]);
};


/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.CutoffConditionType = {
  UNKNOWN_CUTOFF_CONDITION_TYPE: 0,
  NO_CLIENT: 1,
  MANUAL: 2,
  OUT_OF_RANGE_SIGNAL: 3,
  HARDWARE_CONNECTED: 4,
  HARDWARE_DISCONNECTED: 5
};

/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.InputType = {
  UNKNOWN_INPUT_TYPE: 0,
  UNCALIBRATED_LOAD_CELL: 1,
  UNCALIBRATED_MAIN_HV_INPUT: 2,
  UNCALIBRATED_HALL_CURRENT: 3,
  UNCALIBRATED_GENERAL_ANALOG_INPUT: 4,
  UNCALIBRATED_PT100: 5,
  VOLTAGE_HV_INPUT: 6,
  VOLTAGE_GENERAL_ANALOG_INPUT: 7,
  CURRENT_HALL_CURRENT: 8,
  FORCE_FX: 9,
  FORCE_FY: 10,
  FORCE_FZ: 11,
  TORQUE_MX: 12,
  TORQUE_MY: 13,
  TORQUE_MZ: 14,
  ROTATION_SPEED_FREQUENCY: 15,
  OUTPUT_VALUE_ESC_SIGNAL: 16,
  OUTPUT_VALUE_SERVO_SIGNAL: 17,
  ACCELEROMETER_AX: 18,
  ACCELEROMETER_AY: 19,
  ACCELEROMETER_AZ: 20,
  ACCELEROMETER_VIBRATION_LEVEL: 21,
  TEMPERATURE_IR: 22,
  TEMPERATURE_PT100: 23,
  AIRSPEED_DIFFERENTIAL_PRESSURE: 24,
  VOLTAGE_VIN: 25
};

/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.OutputProtocolType = {
  UNKNOWN_OUTPUT_PROTOCOL: 0,
  STANDARD_PWM_50_HZ: 1,
  STANDARD_PWM_100_HZ: 2,
  STANDARD_PWM_200_HZ: 3,
  STANDARD_PWM_300_HZ: 4,
  STANDARD_PWM_400_HZ: 5,
  STANDARD_PWM_500_HZ: 6,
  DSHOT_150: 7,
  DSHOT_300: 8,
  DSHOT_600: 9,
  DSHOT_1200: 10,
  ONESHOT_42: 11,
  ONESHOT_125: 12,
  MULTISHOT: 13
};

/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.OutputType = {
  UNKNOWN_OUTPUT_TYPE: 0,
  ESC: 1,
  SERVO: 2
};

/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.PowertrainSignalType = {
  UNKNOWN_POWERTRAIN_SIGNAL_TYPE: 0,
  ELECTRICAL_POWER_WATT: 1,
  MECHANICAL_POWER_WATT: 2,
  MOTOR_AND_ESC_EFFICIENCY_RATIO: 3,
  PROPELLER_EFFICIENCY_NEWTON_PER_WATT: 4,
  POWERTRAIN_EFFICIENCY_NEWTON_PER_WATT: 5
};

/**
 * @enum {number}
 */
proto.tytorobotics.flightstand.v1.CoreSignalType = {
  UNKNOWN_CORE_SIGNAL_TYPE: 0,
  SUM_POWERTRAINS_THRUST_NEWTON: 6,
  SUM_POWERTRAINS_ELECTRICAL_POWER_WATT: 7,
  SUM_POWERTRAINS_MECHANICAL_POWER_WATT: 8,
  COMBINED_POWERTRAINS_MOTOR_AND_ESC_EFFICIENCY_RATIO: 9,
  COMBINED_POWERTRAINS_PROPELLER_EFFICIENCY_NEWTON_PER_WATT: 10,
  COMBINED_POWERTRAINS_EFFICIENCY_NEWTON_PER_WATT: 11
};

goog.object.extend(exports, proto.tytorobotics.flightstand.v1);
