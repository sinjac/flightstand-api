// This example is incomplete but gives some code to get started. We recommend running and understanding the Python
// example first.
// The user must install the node packages required:
// "@grpc/grpc-js": "^1.5.9"
// "google-protobuf": "^3.15.8",

import messages from "./proto/flight_stand_api_v1_pb";
import services from "./proto/flight_stand_api_v1_grpc_pb";
import {credentials} from "@grpc/grpc-js";

// The proto well known messages, compiled to js:
// https://developers.google.com/protocol-buffers/docs/reference/google.protobuf
import messageFieldMask from "google-protobuf/google/protobuf/field_mask_pb";
import messageDuration from "google-protobuf/google/protobuf/duration_pb";
import messageTimestamp from "google-protobuf/google/protobuf/timestamp_pb";

// Connect to the core
const target = "localhost:50051";
// https://grpc.github.io/grpc/node/grpc.Client.html
const stub = new services.FlightStandClient(target, credentials.createInsecure());

// rpc GetCore(GetCoreRequest) returns (Core);
const request = new messages.GetCoreRequest();
const timeout = {deadline: Date.now() + 5000} // We give a 5 second timeout to get the reply
stub.getCore(request, timeout, (err, core) => {
    console.log(err, core);
});