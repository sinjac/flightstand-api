# Compile javascript files from the .proto
Note, the instructions below are for Tyto Robotics employees. Users of the API don't need to recompile the language files since they are already available in this project.

## Setup dependencies
Ensure node v.14.16.1 is available. it is highly recommended to manage node versions using nvm for Windows. Confirm installation is correct by typing in the terminal:

`node -v`
v.14.16.1


`npm -v`
v6.14.12

`npm install -g grpc-tools@1.11.3`
Note, v1.11.3 is required until [this is resolved](https://github.com/grpc/grpc-node/issues/2338).

## Compile proto

1) From terminal, go to root folder of this project
2) Type `grpc_tools_node_protoc --proto_path=proto --js_out=import_style=commonjs:languages/js --grpc_out=grpc_js:languages/js flight_stand_api_v1.proto`

If you get "running scripts is disabled on this system" errors:
- https://stackoverflow.com/questions/63423584
