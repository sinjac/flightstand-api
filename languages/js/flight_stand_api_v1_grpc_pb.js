// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var flight_stand_api_v1_pb = require('./flight_stand_api_v1_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
var google_protobuf_duration_pb = require('google-protobuf/google/protobuf/duration_pb.js');
var google_protobuf_field_mask_pb = require('google-protobuf/google/protobuf/field_mask_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Board(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Board)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Board');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Board(buffer_arg) {
  return flight_stand_api_v1_pb.Board.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_BoardParameter(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.BoardParameter)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.BoardParameter');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_BoardParameter(buffer_arg) {
  return flight_stand_api_v1_pb.BoardParameter.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearCutoffRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearCutoffRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearCutoffRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearCutoffRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ClearCutoffRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearCutoffResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearCutoffResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearCutoffResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearCutoffResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ClearCutoffResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearPowertrainRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearPowertrainRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearPowertrainRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearPowertrainRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ClearPowertrainRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearPowertrainResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearPowertrainResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearPowertrainResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearPowertrainResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ClearPowertrainResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ClearTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearTestRecorderResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearTestRecorderResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearTestRecorderResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearTestRecorderResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ClearTestRecorderResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearTimedActionSequenceRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearTimedActionSequenceRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ClearTimedActionSequenceRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ClearTimedActionSequenceResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ClearTimedActionSequenceResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ClearTimedActionSequenceResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Core(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Core)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Core');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Core(buffer_arg) {
  return flight_stand_api_v1_pb.Core.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_CreateExternalBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.CreateExternalBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.CreateExternalBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_CreateExternalBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.CreateExternalBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_CreateSimulatedBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.CreateSimulatedBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.CreateSimulatedBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_CreateSimulatedBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.CreateSimulatedBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_CreateSimulatedBoardResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.CreateSimulatedBoardResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.CreateSimulatedBoardResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_CreateSimulatedBoardResponse(buffer_arg) {
  return flight_stand_api_v1_pb.CreateSimulatedBoardResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DeleteTestRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DeleteTestRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DeleteTestRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DeleteTestRequest(buffer_arg) {
  return flight_stand_api_v1_pb.DeleteTestRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DevClearBoardParametersRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DevClearBoardParametersRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DevClearBoardParametersRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DevClearBoardParametersRequest(buffer_arg) {
  return flight_stand_api_v1_pb.DevClearBoardParametersRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DevClearBoardParametersResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DevClearBoardParametersResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DevClearBoardParametersResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DevClearBoardParametersResponse(buffer_arg) {
  return flight_stand_api_v1_pb.DevClearBoardParametersResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DevListBoardParametersRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DevListBoardParametersRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DevListBoardParametersRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DevListBoardParametersRequest(buffer_arg) {
  return flight_stand_api_v1_pb.DevListBoardParametersRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DevListBoardParametersResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DevListBoardParametersResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DevListBoardParametersResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DevListBoardParametersResponse(buffer_arg) {
  return flight_stand_api_v1_pb.DevListBoardParametersResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DevUpdateBoardParameterRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DevUpdateBoardParameterRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DevUpdateBoardParameterRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DevUpdateBoardParameterRequest(buffer_arg) {
  return flight_stand_api_v1_pb.DevUpdateBoardParameterRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DisconnectBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DisconnectBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DisconnectBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DisconnectBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.DisconnectBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_DisconnectBoardResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.DisconnectBoardResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.DisconnectBoardResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_DisconnectBoardResponse(buffer_arg) {
  return flight_stand_api_v1_pb.DisconnectBoardResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_FlashBoardFirmwareRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.FlashBoardFirmwareRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.FlashBoardFirmwareRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_FlashBoardFirmwareRequest(buffer_arg) {
  return flight_stand_api_v1_pb.FlashBoardFirmwareRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_FlashBoardFirmwareResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.FlashBoardFirmwareResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.FlashBoardFirmwareResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_FlashBoardFirmwareResponse(buffer_arg) {
  return flight_stand_api_v1_pb.FlashBoardFirmwareResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetCoreRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetCoreRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetCoreRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetCoreRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetCoreRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetInputRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetInputRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetInputRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetInputRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetInputRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetOutputRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetOutputRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetOutputRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetOutputRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetOutputRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetPowertrainRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetPowertrainRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetPowertrainRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetPowertrainRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetPowertrainRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetServerStatusRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetServerStatusRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetServerStatusRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetServerStatusRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetServerStatusRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetSignalRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetSignalRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetSignalRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetSignalRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetSignalRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_GetTestRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.GetTestRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.GetTestRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_GetTestRequest(buffer_arg) {
  return flight_stand_api_v1_pb.GetTestRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_IdentifyBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.IdentifyBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.IdentifyBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_IdentifyBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.IdentifyBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_IdentifyBoardResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.IdentifyBoardResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.IdentifyBoardResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_IdentifyBoardResponse(buffer_arg) {
  return flight_stand_api_v1_pb.IdentifyBoardResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Input(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Input)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Input');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Input(buffer_arg) {
  return flight_stand_api_v1_pb.Input.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListBoardsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListBoardsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListBoardsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListBoardsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListBoardsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListBoardsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListBoardsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListBoardsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListBoardsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListBoardsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListCutoffConditionsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListCutoffConditionsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListCutoffConditionsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListCutoffConditionsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListCutoffConditionsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListCutoffConditionsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListCutoffConditionsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListCutoffConditionsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListCutoffConditionsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListCutoffConditionsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListInputsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListInputsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListInputsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListInputsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListInputsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListInputsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListInputsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListInputsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListInputsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListInputsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListOutputsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListOutputsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListOutputsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListOutputsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListOutputsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListOutputsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListOutputsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListOutputsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListOutputsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListOutputsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListPowertrainsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListPowertrainsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListPowertrainsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListPowertrainsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListPowertrainsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListPowertrainsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListPowertrainsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListPowertrainsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListPowertrainsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListPowertrainsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSamplesRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSamplesRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSamplesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSamplesRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListSamplesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSamplesResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSamplesResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSamplesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSamplesResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListSamplesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSignalStatsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSignalStatsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSignalStatsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSignalStatsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListSignalStatsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSignalStatsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSignalStatsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSignalStatsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSignalStatsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListSignalStatsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSignalsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSignalsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSignalsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSignalsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListSignalsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListSignalsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListSignalsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListSignalsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListSignalsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListSignalsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListTestSamplesRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListTestSamplesRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListTestSamplesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListTestSamplesRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListTestSamplesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListTestSamplesResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListTestSamplesResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListTestSamplesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListTestSamplesResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListTestSamplesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListTestsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListTestsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListTestsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListTestsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ListTestsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ListTestsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ListTestsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ListTestsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ListTestsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ListTestsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Output(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Output)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Output');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Output(buffer_arg) {
  return flight_stand_api_v1_pb.Output.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Powertrain(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Powertrain)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Powertrain');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Powertrain(buffer_arg) {
  return flight_stand_api_v1_pb.Powertrain.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ResetBoardRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ResetBoardRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ResetBoardRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ResetBoardRequest(buffer_arg) {
  return flight_stand_api_v1_pb.ResetBoardRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ResetBoardResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ResetBoardResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ResetBoardResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ResetBoardResponse(buffer_arg) {
  return flight_stand_api_v1_pb.ResetBoardResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_SaveTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.SaveTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.SaveTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_SaveTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.SaveTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_SaveTestRecorderResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.SaveTestRecorderResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.SaveTestRecorderResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_SaveTestRecorderResponse(buffer_arg) {
  return flight_stand_api_v1_pb.SaveTestRecorderResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_ServerStatus(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.ServerStatus)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.ServerStatus');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_ServerStatus(buffer_arg) {
  return flight_stand_api_v1_pb.ServerStatus.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Signal(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Signal)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Signal');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Signal(buffer_arg) {
  return flight_stand_api_v1_pb.Signal.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_SyncTimestampsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.SyncTimestampsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.SyncTimestampsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_SyncTimestampsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.SyncTimestampsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_SyncTimestampsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.SyncTimestampsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.SyncTimestampsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_SyncTimestampsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.SyncTimestampsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TakeSampleTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TakeSampleTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.TakeSampleTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TakeSampleTestRecorderResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TakeSampleTestRecorderResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderResponse(buffer_arg) {
  return flight_stand_api_v1_pb.TakeSampleTestRecorderResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TareInputsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TareInputsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TareInputsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TareInputsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.TareInputsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TareInputsResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TareInputsResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TareInputsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TareInputsResponse(buffer_arg) {
  return flight_stand_api_v1_pb.TareInputsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_Test(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.Test)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.Test');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_Test(buffer_arg) {
  return flight_stand_api_v1_pb.Test.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TestRecorder(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TestRecorder)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TestRecorder');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TestRecorder(buffer_arg) {
  return flight_stand_api_v1_pb.TestRecorder.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TimedActionSequence(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TimedActionSequence)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TimedActionSequence');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TimedActionSequence(buffer_arg) {
  return flight_stand_api_v1_pb.TimedActionSequence.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TriggerCutoffRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TriggerCutoffRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TriggerCutoffRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TriggerCutoffRequest(buffer_arg) {
  return flight_stand_api_v1_pb.TriggerCutoffRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_TriggerCutoffResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.TriggerCutoffResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.TriggerCutoffResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_TriggerCutoffResponse(buffer_arg) {
  return flight_stand_api_v1_pb.TriggerCutoffResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateCoreRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateCoreRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateCoreRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateCoreRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateCoreRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateInputRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateInputRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateInputRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateInputRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateInputRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateOutputRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateOutputRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateOutputRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateOutputRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateOutputRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdatePowertrainRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdatePowertrainRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdatePowertrainRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdatePowertrainRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdatePowertrainRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateTestRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateTestRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateTestRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateTestRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateTestRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_UpdateTimedActionSequenceRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.UpdateTimedActionSequenceRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.UpdateTimedActionSequenceRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_UpdateTimedActionSequenceRequest(buffer_arg) {
  return flight_stand_api_v1_pb.UpdateTimedActionSequenceRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchBoardsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchBoardsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchBoardsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchBoardsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchBoardsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchCoreRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchCoreRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchCoreRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchCoreRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchCoreRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchInputsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchInputsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchInputsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchInputsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchInputsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchOutputsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchOutputsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchOutputsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchOutputsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchOutputsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchPowertrainsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchPowertrainsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchPowertrainsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchPowertrainsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchPowertrainsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchSamplesRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchSamplesRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchSamplesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchSamplesRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchSamplesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchSamplesResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchSamplesResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchSamplesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchSamplesResponse(buffer_arg) {
  return flight_stand_api_v1_pb.WatchSamplesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchSignalsRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchSignalsRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchSignalsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchSignalsRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchSignalsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WatchTestRecorderRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WatchTestRecorderRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WatchTestRecorderRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WatchTestRecorderRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WatchTestRecorderRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WriteExternalSamplesRequest(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WriteExternalSamplesRequest)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WriteExternalSamplesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WriteExternalSamplesRequest(buffer_arg) {
  return flight_stand_api_v1_pb.WriteExternalSamplesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tytorobotics_flightstand_v1_WriteExternalSamplesResponse(arg) {
  if (!(arg instanceof flight_stand_api_v1_pb.WriteExternalSamplesResponse)) {
    throw new Error('Expected argument of type tytorobotics.flightstand.v1.WriteExternalSamplesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tytorobotics_flightstand_v1_WriteExternalSamplesResponse(buffer_arg) {
  return flight_stand_api_v1_pb.WriteExternalSamplesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var FlightStandService = exports.FlightStandService = {
  // Returns the server status. Poll this function from the client regularly. If this function is not called at least
// once per second, the core will trigger a cutoff. Calling this function every 1/2 second will work. Only one client
// is required to perform this poll. If the GUI is running, the user script does not have to regularly call this rpc.
getServerStatus: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetServerStatus',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetServerStatusRequest,
    responseType: flight_stand_api_v1_pb.ServerStatus,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetServerStatusRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetServerStatusRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ServerStatus,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ServerStatus,
  },
  // Returns the core information
getCore: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetCore',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetCoreRequest,
    responseType: flight_stand_api_v1_pb.Core,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetCoreRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetCoreRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Core,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Core,
  },
  // Make changes to the core
updateCore: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateCore',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateCoreRequest,
    responseType: flight_stand_api_v1_pb.Core,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateCoreRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateCoreRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Core,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Core,
  },
  // Watches any changes on the core. When first called, the current Core is returned.
watchCore: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchCore',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchCoreRequest,
    responseType: flight_stand_api_v1_pb.Core,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchCoreRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchCoreRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Core,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Core,
  },
  // Lists cutoff conditions that prevent the cutoff from being reset.
listCutoffConditions: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListCutoffConditions',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListCutoffConditionsRequest,
    responseType: flight_stand_api_v1_pb.ListCutoffConditionsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListCutoffConditionsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListCutoffConditionsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListCutoffConditionsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListCutoffConditionsResponse,
  },
  // Triggers a manual safety cutoff.
triggerCutoff: {
    path: '/tytorobotics.flightstand.v1.FlightStand/TriggerCutoff',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.TriggerCutoffRequest,
    responseType: flight_stand_api_v1_pb.TriggerCutoffResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_TriggerCutoffRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_TriggerCutoffRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TriggerCutoffResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TriggerCutoffResponse,
  },
  // Clears a safety cutoff. Although no error will be returned, this will only work if there are no active cutoff
// conditions. It is possible a signal becomes out-of-range immediately after reset, triggering a new cutoff
// immediately after.
clearCutoff: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ClearCutoff',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ClearCutoffRequest,
    responseType: flight_stand_api_v1_pb.ClearCutoffResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ClearCutoffRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ClearCutoffRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ClearCutoffResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ClearCutoffResponse,
  },
  // Triggers a manual timestamp sync. This helps with debugging.
syncTimestamps: {
    path: '/tytorobotics.flightstand.v1.FlightStand/SyncTimestamps',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.SyncTimestampsRequest,
    responseType: flight_stand_api_v1_pb.SyncTimestampsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_SyncTimestampsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_SyncTimestampsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_SyncTimestampsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_SyncTimestampsResponse,
  },
  // Gets a board
getBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetBoardRequest,
    responseType: flight_stand_api_v1_pb.Board,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Board,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Board,
  },
  // Lists available boards (those that successfully connected and identified)
listBoards: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListBoards',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListBoardsRequest,
    responseType: flight_stand_api_v1_pb.ListBoardsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListBoardsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListBoardsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListBoardsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListBoardsResponse,
  },
  // Watch boards. When first called, each board is streamed once.
watchBoards: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchBoards',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchBoardsRequest,
    responseType: flight_stand_api_v1_pb.Board,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchBoardsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchBoardsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Board,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Board,
  },
  // Resets a board. The core will automatically attempt to reconnect after the reset. This RPC may take a few seconds
// depending on the hardware implementation.
resetBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ResetBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ResetBoardRequest,
    responseType: flight_stand_api_v1_pb.ResetBoardResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ResetBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ResetBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ResetBoardResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ResetBoardResponse,
  },
  // Identify a board will flash the light of the board in red, green, blue and white for a couple seconds.
identifyBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/IdentifyBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.IdentifyBoardRequest,
    responseType: flight_stand_api_v1_pb.IdentifyBoardResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_IdentifyBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_IdentifyBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_IdentifyBoardResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_IdentifyBoardResponse,
  },
  // Manually disconnect a board. Closes the board connection and releases resources (eg. COM port).
disconnectBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/DisconnectBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.DisconnectBoardRequest,
    responseType: flight_stand_api_v1_pb.DisconnectBoardResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_DisconnectBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_DisconnectBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_DisconnectBoardResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_DisconnectBoardResponse,
  },
  // Add a simulated boards with virtual hardware. At this time, the simulated board cannot be customized. It simulates
// a small motor/propeller setup generating near 200 grams of thrust at full throttle.
createSimulatedBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/CreateSimulatedBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.CreateSimulatedBoardRequest,
    responseType: flight_stand_api_v1_pb.CreateSimulatedBoardResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_CreateSimulatedBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_CreateSimulatedBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_CreateSimulatedBoardResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_CreateSimulatedBoardResponse,
  },
  // Note: this feature requires a free license key. Contact support@tytorobotics.com to obtain one.
// Add an external board that fully interacts with the Flight Stand Software. This enables the user to interface
// external inputs and outputs, essentially allowing the control of CANbus ESCs and to add custom sensors and outputs.
// Once an external board is created, use the WriteExternalSamples rpc to update external sensor data, and use the
// WatchOutputs rpc to react to changes on the output targets. When reacting to changes on the output targets, you
// must take into account the rate limiter imposed by the user, Provide feedback the output reacted through the
// WriteExternalSamples rpc. See examples in the Python documentation for more information.
createExternalBoard: {
    path: '/tytorobotics.flightstand.v1.FlightStand/CreateExternalBoard',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.CreateExternalBoardRequest,
    responseType: flight_stand_api_v1_pb.Board,
    requestSerialize: serialize_tytorobotics_flightstand_v1_CreateExternalBoardRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_CreateExternalBoardRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Board,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Board,
  },
  // Flashes the firmware on a board. The core has the correct supported firmware in its source already. The core will
// handle all the logic to reset, disconnect, flash, reconnect, etc...
flashBoardFirmware: {
    path: '/tytorobotics.flightstand.v1.FlightStand/FlashBoardFirmware',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.FlashBoardFirmwareRequest,
    responseType: flight_stand_api_v1_pb.FlashBoardFirmwareResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_FlashBoardFirmwareRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_FlashBoardFirmwareRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_FlashBoardFirmwareResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_FlashBoardFirmwareResponse,
  },
  // Gets an input
getInput: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetInput',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetInputRequest,
    responseType: flight_stand_api_v1_pb.Input,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetInputRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetInputRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Input,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Input,
  },
  // Updates an input
updateInput: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateInput',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateInputRequest,
    responseType: flight_stand_api_v1_pb.Input,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateInputRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateInputRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Input,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Input,
  },
  // Lists available inputs. Does not return closed inputs
listInputs: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListInputs',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListInputsRequest,
    responseType: flight_stand_api_v1_pb.ListInputsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListInputsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListInputsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListInputsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListInputsResponse,
  },
  // Watches inputs for changes (other than the values). When first called, each available input is streamed once.
watchInputs: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchInputs',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchInputsRequest,
    responseType: flight_stand_api_v1_pb.Input,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchInputsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchInputsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Input,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Input,
  },
  // Tares all inputs on all boards that can be tared, for example load cells.
tareInputs: {
    path: '/tytorobotics.flightstand.v1.FlightStand/TareInputs',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.TareInputsRequest,
    responseType: flight_stand_api_v1_pb.TareInputsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_TareInputsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_TareInputsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TareInputsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TareInputsResponse,
  },
  // Returns the latest Sample values.
listSamples: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListSamples',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListSamplesRequest,
    responseType: flight_stand_api_v1_pb.ListSamplesResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListSamplesRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListSamplesRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListSamplesResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListSamplesResponse,
  },
  // Regularly get Sample updates. This RPC is designed to provide real-time updates for the Flight Stand GUI's plots.
// Unlike polling with ListSamples, this method uses an internal buffer that accumulates updates (up to a reasonable
// limit) when the client temporarily slows down, ensuring that the client won't miss any updates. This is especially
// useful when the GUI freezes occasionally due to actions like docking or undocking windows. The network stream is
// also buffered, and a response is sent only when a request is received to prevent lag. It's important to note that
// this RPC has been heavily optimized for use with the Flight Stand GUI. The sample rate is fixed at 10 Hz and
// cannot be configured. The RPC has been crafted and optimized to provide smooth real-time plots in the GUI while
// keeping RAM and CPU usage within reasonable limits. To prevent the send buffer from filling up, the RPC employs
// techniques similar to those used in real-time video conferencing. The idea is to limit latency by limiting the
// amount of buffering that can be done. To achieve this, the client must acknowledge receipt of the data before the
// RPC will send more data. In summary, it's required to use this RPC as a bidirectional stream, as it expects the GUI
// to respond after each update.
watchSamples: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchSamples',
    requestStream: true,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchSamplesRequest,
    responseType: flight_stand_api_v1_pb.WatchSamplesResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchSamplesRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchSamplesRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_WatchSamplesResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_WatchSamplesResponse,
  },
  // Writes external input samples. This rpc only works on external inputs added using the CreateExternalBoard rpc.
// When creating the Sample, assign the input's raw_signal_name field to the Sample's signal_name field.
writeExternalSamples: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WriteExternalSamples',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.WriteExternalSamplesRequest,
    responseType: flight_stand_api_v1_pb.WriteExternalSamplesResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WriteExternalSamplesRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WriteExternalSamplesRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_WriteExternalSamplesResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_WriteExternalSamplesResponse,
  },
  // Gets a signal. See the "Signal" message for more information about what a signal represents.
getSignal: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetSignal',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetSignalRequest,
    responseType: flight_stand_api_v1_pb.Signal,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetSignalRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetSignalRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Signal,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Signal,
  },
  // List all signals.
listSignals: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListSignals',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListSignalsRequest,
    responseType: flight_stand_api_v1_pb.ListSignalsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListSignalsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListSignalsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListSignalsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListSignalsResponse,
  },
  // Watch signals for changes. When first called, all signals are streamed once.
watchSignals: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchSignals',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchSignalsRequest,
    responseType: flight_stand_api_v1_pb.Signal,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchSignalsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchSignalsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Signal,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Signal,
  },
  // List signal stats. Gets signal performance metrics, like update rate.
listSignalStats: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListSignalStats',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListSignalStatsRequest,
    responseType: flight_stand_api_v1_pb.ListSignalStatsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListSignalStatsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListSignalStatsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListSignalStatsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListSignalStatsResponse,
  },
  // Get an output's configuration (cutoff, range, alias, protocol, etc...).
getOutput: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetOutput',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetOutputRequest,
    responseType: flight_stand_api_v1_pb.Output,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetOutputRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetOutputRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Output,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Output,
  },
  // Update an output's configuration or target value.
updateOutput: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateOutput',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateOutputRequest,
    responseType: flight_stand_api_v1_pb.Output,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateOutputRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateOutputRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Output,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Output,
  },
  // List all outputs. Does not return closed outputs.
listOutputs: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListOutputs',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListOutputsRequest,
    responseType: flight_stand_api_v1_pb.ListOutputsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListOutputsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListOutputsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListOutputsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListOutputsResponse,
  },
  // Watch outputs for changes of configuration, other than OutputSignal changes.
watchOutputs: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchOutputs',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchOutputsRequest,
    responseType: flight_stand_api_v1_pb.Output,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchOutputsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchOutputsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Output,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Output,
  },
  // Updates the action sequence (appends to previous sequence). This feature is related to automatic control. It allows
// controlling outputs in real-time at up to 1000 hz by specifying the update time. Compared to updating outputs
// directly using the UpdateOutput method, this method avoids issues with network/USB communication latency and
// limited rate of updates. Using this method ensures the outputs are updated at the correct time within the hardware.
updateTimedActionSequence: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateTimedActionSequence',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateTimedActionSequenceRequest,
    responseType: flight_stand_api_v1_pb.TimedActionSequence,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateTimedActionSequenceRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateTimedActionSequenceRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TimedActionSequence,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TimedActionSequence,
  },
  // Clears the action sequence across all devices. This will prevent future updates from happening.
clearTimedActionSequence: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ClearTimedActionSequence',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ClearTimedActionSequenceRequest,
    responseType: flight_stand_api_v1_pb.ClearTimedActionSequenceResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ClearTimedActionSequenceResponse,
  },
  // Gets a powertrain. A powertrain represents a combination of a motor, a propeller, and an ESC. Each powertrain
// should be mapped with corresponding hardware sensors and outputs. The core will automatically calculate derived
// signals when possible. For example, if both a current and a voltage sensors are mapped, an "electrical power" is
// derived signal is generated.
getPowertrain: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetPowertrain',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetPowertrainRequest,
    responseType: flight_stand_api_v1_pb.Powertrain,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetPowertrainRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetPowertrainRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Powertrain,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Powertrain,
  },
  // Updates a powertrain.
updatePowertrain: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdatePowertrain',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdatePowertrainRequest,
    responseType: flight_stand_api_v1_pb.Powertrain,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdatePowertrainRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdatePowertrainRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Powertrain,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Powertrain,
  },
  // Lists all powertrains.
listPowertrains: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListPowertrains',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListPowertrainsRequest,
    responseType: flight_stand_api_v1_pb.ListPowertrainsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListPowertrainsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListPowertrainsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListPowertrainsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListPowertrainsResponse,
  },
  // Clear a powertrain (removes every mapping and component names).
clearPowertrain: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ClearPowertrain',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ClearPowertrainRequest,
    responseType: flight_stand_api_v1_pb.ClearPowertrainResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ClearPowertrainRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ClearPowertrainRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ClearPowertrainResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ClearPowertrainResponse,
  },
  // Watch changes on powertrains.
watchPowertrains: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchPowertrains',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchPowertrainsRequest,
    responseType: flight_stand_api_v1_pb.Powertrain,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchPowertrainsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchPowertrainsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Powertrain,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Powertrain,
  },
  // Gets the test recorder, used to prepare a test before saving.
getTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetTestRecorder',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.TestRecorder,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TestRecorder,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TestRecorder,
  },
  // Updates the test recorder.
updateTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateTestRecorder',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.TestRecorder,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TestRecorder,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TestRecorder,
  },
  // Watch the test recorder for changes.
watchTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/WatchTestRecorder',
    requestStream: false,
    responseStream: true,
    requestType: flight_stand_api_v1_pb.WatchTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.TestRecorder,
    requestSerialize: serialize_tytorobotics_flightstand_v1_WatchTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_WatchTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TestRecorder,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TestRecorder,
  },
  // Take a data sample.
takeSampleTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/TakeSampleTestRecorder',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.TakeSampleTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.TakeSampleTestRecorderResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_TakeSampleTestRecorderResponse,
  },
  // Discard changes and start a new test recorder.
clearTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ClearTestRecorder',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ClearTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.ClearTestRecorderResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ClearTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ClearTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ClearTestRecorderResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ClearTestRecorderResponse,
  },
  // Save the test recorder as a new Test.
saveTestRecorder: {
    path: '/tytorobotics.flightstand.v1.FlightStand/SaveTestRecorder',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.SaveTestRecorderRequest,
    responseType: flight_stand_api_v1_pb.SaveTestRecorderResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_SaveTestRecorderRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_SaveTestRecorderRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_SaveTestRecorderResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_SaveTestRecorderResponse,
  },
  // Gets a Test.
getTest: {
    path: '/tytorobotics.flightstand.v1.FlightStand/GetTest',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.GetTestRequest,
    responseType: flight_stand_api_v1_pb.Test,
    requestSerialize: serialize_tytorobotics_flightstand_v1_GetTestRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_GetTestRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Test,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Test,
  },
  // Update a Test.
updateTest: {
    path: '/tytorobotics.flightstand.v1.FlightStand/UpdateTest',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.UpdateTestRequest,
    responseType: flight_stand_api_v1_pb.Test,
    requestSerialize: serialize_tytorobotics_flightstand_v1_UpdateTestRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_UpdateTestRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_Test,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_Test,
  },
  // List all tests.
listTests: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListTests',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListTestsRequest,
    responseType: flight_stand_api_v1_pb.ListTestsResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListTestsRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListTestsRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListTestsResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListTestsResponse,
  },
  // Delete a test.
deleteTest: {
    path: '/tytorobotics.flightstand.v1.FlightStand/DeleteTest',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.DeleteTestRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tytorobotics_flightstand_v1_DeleteTestRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_DeleteTestRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  // Retrieve the recorded data for a test (may be interpolated to reduce the amount of data). The data is returned with
// timestamps in order. The deadline specified in the context of the API call will force an early return even if the
// length of the returned data has not reached pageSize. This is useful to display progress bars.
listTestData: {
    path: '/tytorobotics.flightstand.v1.FlightStand/ListTestData',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.ListTestSamplesRequest,
    responseType: flight_stand_api_v1_pb.ListTestSamplesResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_ListTestSamplesRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_ListTestSamplesRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_ListTestSamplesResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_ListTestSamplesResponse,
  },
  // Lists all board parameters. This function is synchronous, and will return after fetching all board parameters from
// the board. Important: for dev purposes ONLY (it bypasses all safety and business logic). Calling this function may
// also have unexpected behavior (may trigger an internal cutoff, stop data streaming, or load default values).
devListBoardParameters: {
    path: '/tytorobotics.flightstand.v1.FlightStand/DevListBoardParameters',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.DevListBoardParametersRequest,
    responseType: flight_stand_api_v1_pb.DevListBoardParametersResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_DevListBoardParametersRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_DevListBoardParametersRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_DevListBoardParametersResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_DevListBoardParametersResponse,
  },
  // Updates a board parameter. For simplicity this update method does not use field masks, so the whole parameter
// must be specified when updating. For dev purposes ONLY (it bypasses all safety and business logic). There is no
// defined behavior on the effect of updating a parameter (changes are not expected to propagate, a board reset may be
// needed for changes to take effect).
devUpdateBoardParameter: {
    path: '/tytorobotics.flightstand.v1.FlightStand/DevUpdateBoardParameter',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.DevUpdateBoardParameterRequest,
    responseType: flight_stand_api_v1_pb.BoardParameter,
    requestSerialize: serialize_tytorobotics_flightstand_v1_DevUpdateBoardParameterRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_DevUpdateBoardParameterRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_BoardParameter,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_BoardParameter,
  },
  // Clears all parameters to 0 value. It is recommended to leave a timeout of 30 seconds for this command to reply.
// For dev purposes ONLY. This will delete all calibration data, and the circuit will become unusable.
devClearBoardParameters: {
    path: '/tytorobotics.flightstand.v1.FlightStand/DevClearBoardParameters',
    requestStream: false,
    responseStream: false,
    requestType: flight_stand_api_v1_pb.DevClearBoardParametersRequest,
    responseType: flight_stand_api_v1_pb.DevClearBoardParametersResponse,
    requestSerialize: serialize_tytorobotics_flightstand_v1_DevClearBoardParametersRequest,
    requestDeserialize: deserialize_tytorobotics_flightstand_v1_DevClearBoardParametersRequest,
    responseSerialize: serialize_tytorobotics_flightstand_v1_DevClearBoardParametersResponse,
    responseDeserialize: deserialize_tytorobotics_flightstand_v1_DevClearBoardParametersResponse,
  },
};

exports.FlightStandClient = grpc.makeGenericClientConstructor(FlightStandService);
