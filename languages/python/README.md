# Setup to run the example.py file
This example was written in [PyCharm with a venv configured](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html), using Python v3.10.
From the PyCharm terminal install the required python packages by running the command:
```bash
$ py -m pip install grpcio grpcio-tools
```

You will also need to install the local python packages. Assuming the terminal is run from this folder, type:
```bash
$ py -m pip install -e .
```
***Important:*** if you get errors while running the above command, it is most likely because you are running the command while the terminal is in the wrong directory. It should be:

`
PS C:\Users\robil\OneDrive\Bureau\git\flightstand-api\languages\python> py -m pip install -e .
`

and not:

`
PS C:\Users\robil\OneDrive\Bureau\git\flightstand-api\> py -m pip install -e .
`


The simulated hardware example should run normally by typing in the terminal:
```bash
$ python ./examples/example.py
```

# Compile Python files from the .proto
Note, the instructions below are for Tyto Robotics employees. Users of the API don't need to recompile the language files since they are already available in this project.

## Setup dependencies
- Download the latest python from [python.org](python.org)
- Install the python packages, run:

```bash
$ py -m pip install grpcio grpcio-tools mypy-protobuf types-protobuf
```

If the program cannot work and print message: cannot find google module after installing grpcio and grpc-tools, try running:
```bash
$ py -m pip install google protobuf
```
## Compile proto
1) From terminal, go to root folder of this project
2) Type (note mypy helps with IDE autocomplete)
```bash
py -m grpc_tools.protoc --proto_path=proto -I. --python_out=languages/python --mypy_out=languages/python --grpc_python_out=languages/python --mypy_grpc_out=languages/python flight_stand_api_v1.proto
```