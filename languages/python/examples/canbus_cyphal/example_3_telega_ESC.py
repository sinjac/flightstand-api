"""

This example shows how to interface a motor controller communicating through the UAVCAN network interface.
In this particular example, we interface with a Myxa ESC running the Telega V1.0 firmware. This example can be adapted
to your particular hardware by changing the relevant lines of code and environment variables. For this example we opt
to use an already tuned and configured propulsion kit that just works, limiting the scope of this guide. Refer to the
manufacturer's documentation to tune and configure your ESC to your specific motor.

See the picture ./example_3_hardware.png. Your setup may vary, we have the following third-party hardware connected:

2 x USB cable:
https://shop.zubax.com/collections/cables-connectors/products/micro-usb-cable-type-b-double-shielded

Zubax Babel USB<->CAN adapter:
https://shop.zubax.com/collections/development-tools/products/zubax-babel

JST 4-pin cable:
https://shop.zubax.com/collections/cables-connectors/products/uavcan-micro-patch-cable-twisted?variant=40066160853091

UAV propulsion kit UPK-MA1-0-SS4006KV320-P15x5:
https://shop.zubax.com/products/uav-propulsion-kit?variant=31749560074339

UAVCAN termination resistor:
https://shop.zubax.com/collections/cables-connectors/products/uavcan-micro-termination-plug

An external power supply for the ESC, set to 15 V:
https://canada.newark.com/tenma/72-7655/power-supply-ac-dc-1-15v-60a-900w/dp/57J0202

Follow these instructions to run the example:

*** SAFETY WARNING: run this example without any propeller installed ***

1) Connect all the hardware on your PC, and ensure the hardware is operational using external tools such as Yukon.
Don't forget to close Yukon or any other external tools using the same COM interface. Here is a useful video to help
setting up a telega ESC, you should be able to make your motor spin from Yukon/Yakut before continuing. This example
follows the same port assignments as in the linked video.
https://www.youtube.com/watch?v=_nGi3y3FqvU

2) To ensure correct software setup, confirm example_1.py is working before trying to run this example.

3) Using the same method explained in example_1, set the environment variables as follows, but update the COM# to match
the COM port used by your Zubax Babel:
CYPHAL_PATH=.\public_regulated_data_types\;.\zubax_dsdl;
UAVCAN__CAN__IFACE=slcan:COM4;
UAVCAN__CAN__MTU=8;
UAVCAN__NODE__ID=42;
UAVCAN__CAN__BITRATE=1000000 1000000;
UAVCAN__SUB__TELEMETRY_DYNAMICS__ID=100;
UAVCAN__SUB__TELEMETRY_POWER__ID=101;
UAVCAN__PUB__CONTROL_TORQUE__ID=111;

5) Run the Flight Stand Software.

6) Right-click in this code and select "Run example_3_telega_ESC" from the context menu. You should see no errors
and some information printed on the console.

7) Using the Flight Stand Software, you should be able to interact with the ESC from the manual control tab. Some
hardware mapping may be required within the Flight Stand Software.

"""

import os
import asyncio
import pycyphal
from flightstand import FlightStand

# DSDL files are automatically compiled by pycyphal import hook from sources pointed by CYPHAL_PATH env variable.
import pycyphal.application

# Import namespaces we're planning to use.
import uavcan.node  # noqa
import zubax
import zubax.physics.dynamics
import zubax.physics.electricity
import zubax.primitive.real16

def print_uavcan_environment_variables():
    print("UAVCAN environment variables:")
    for name, value in os.environ.items():
        if name.startswith("UAVCAN") or name.startswith("CYPHAL"):
            print(f"{name}: {value}")
    print()


# Function to handle measurements from UAVCAN and forward them to the Flight Stand Software
# See https://github.com/Zubax/zubax_dsdl/tree/master/zubax/physics/electricity for details
def handle_power_measurement(fs, fs_current_input, fs_voltage_input, node, port_name):
    def received_new_value(msg: zubax.physics.electricity.PowerTs_1, _metadata):
        # Send the new measurements to the Flight Stand Software.
        current_value = float(msg.value.current.ampere)
        voltage_value = float(msg.value.voltage.volt)
        current_sample = fs.create_sample(fs_current_input, current_value)
        voltage_sample = fs.create_sample(fs_voltage_input, voltage_value)
        fs.write_external_samples([current_sample, voltage_sample])

    # To receive data from the UAVCAN network, create a subscriber.
    subscriber = node.make_subscriber(zubax.physics.electricity.PowerTs_1, port_name)
    subscriber.receive_in_background(received_new_value)


# Function to handle measurements from UAVCAN and forward them to the Flight Stand Software
# See https://github.com/Zubax/zubax_dsdl/tree/master/zubax/physics/dynamics for details
def handle_dynamics_measurement(fs, fs_rotation_speed_input, node, port_name):
    def received_new_value(msg: zubax.physics.dynamics.DoF3rdTs_1, _metadata):
        # Send the new measurements to the Flight Stand Software.
        rotation_speed_value = float(msg.value.kinematics.base.velocity)
        rotation_speed_sample = fs.create_sample(fs_rotation_speed_input, rotation_speed_value)
        fs.write_external_samples([rotation_speed_sample])

    # To receive data from the UAVCAN network, create a subscriber.
    subscriber = node.make_subscriber(zubax.physics.dynamics.DoF3rdTs_1, port_name)
    subscriber.receive_in_background(received_new_value)


async def handle_control_output(fs: FlightStand, fs_output: FlightStand.Proto.Output,
                                pub: pycyphal.presentation.Publisher):
    # Get the value the output should be at (as commanded by the Flight Stand Software).
    torque_output, active = fs.get_external_output_state(fs_output)

    # Send it to the UAVCAN network
    if active:
        # See https://github.com/Zubax/zubax_dsdl/blob/master/zubax/primitive/real16/Vector31.1.0.dsdl for format
        data = [0] * 31
        data[0] = torque_output
        vector = zubax.primitive.real16.Vector31_1(data)
        await pub.publish(vector)


def configure_flight_stand():
    # Define external inputs and outputs for the Flight Stand Software. See the external_hardware_interface.py for
    # more details about using this feature.
    fs = FlightStand()
    external_input_definitions = [
        # Telemetry data available from the ESC we will be using
        fs.Proto.ExternalInput(input_type=fs.Proto.ROTATION_SPEED_FREQUENCY),
        fs.Proto.ExternalInput(input_type=fs.Proto.CURRENT_HALL_CURRENT),
        fs.Proto.ExternalInput(input_type=fs.Proto.VOLTAGE_HV_INPUT)
    ]
    external_output_definitions = [
        # We control this ESC by torque commands in this example
        fs.Proto.ExternalOutput(
            output_type=fs.Proto.ESC,
            min_system_value=0,
            max_system_value=0.1  # The max torque value you want to allow. For this example, we use a low value.
        )
    ]

    # Create the external board
    external_board = fs.connect_external_board(external_input_definitions, external_output_definitions,
                                               display_name="Zubax Myxa ESC",
                                               model_number="MYXA-A1")
    external_inputs = fs.list_inputs(board=external_board)
    external_outputs = fs.list_outputs(board=external_board)

    return fs, external_inputs, external_outputs


def run_uavcan_measurement_handlers(fs, external_inputs, uavcan_node):
    # Receive measurements from the UAVCAN network and forward them to the Flight Stand Software
    handle_dynamics_measurement(fs, external_inputs[0], uavcan_node, "telemetry_dynamics")
    handle_power_measurement(fs, external_inputs[1], external_inputs[2], uavcan_node, "telemetry_power")


async def main():
    # Helps with troubleshooting
    print_uavcan_environment_variables()

    # Configure the FlightStand Software
    fs, external_inputs, external_outputs = configure_flight_stand()

    # Start the UAVCAN network connection
    with pycyphal.application.make_node(pycyphal.application.NodeInfo(name="demo.main")) as uavcan_node:
        # Receive measurements from the UAVCAN network and forward them to the Flight Stand Software
        run_uavcan_measurement_handlers(fs, external_inputs, uavcan_node)

        # To send data to the UAVCAN network, you create a publisher.
        pub_torque = uavcan_node.make_publisher(zubax.primitive.real16.Vector31_1, "control_torque")

        # Continuously run a loop to receive control updates from the Flight Stand Software
        print("Running. See the external board created in the Flight Stand Software.")
        while True:
            await asyncio.sleep(0.1)
            await handle_control_output(fs, external_outputs[0], pub_torque)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
