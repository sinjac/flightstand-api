# This example shows how to interact with the Flight Stand data recording features. To run this example
# script, please connect some hardware (or simulated hardware) to the Flight Stand. Data recording is the only
# way to reach true 1000 Hz sample rate for the sensors that have 1000 Hz sample rate. This is because the flight stand
# hardware sends its data in chunks.

import time

from flightstand import FlightStand

print(" **** Running example script in Python **** ")
core = FlightStand()

if len(core.list_boards()) == 0:
    print("No hardware connected, cannot run this example")
    exit()

# Clear existing test data
core.clear_test()

# Take five consecutive data samples on the current test
core.take_sample()
core.take_sample()
core.take_sample()
core.take_sample()
core.take_sample()

# Get the test recorder
test_recorder = core.get_test_recorder()
print("Manual samples taken: " + str(test_recorder.manual_samples_count))

# Start continuous recording
test_recorder.is_continuous_recording = True
core.update_test_recorder(test_recorder, ['is_continuous_recording'])

# While we continuously record, we simultaneously take samples
time.sleep(1)
core.take_sample()
time.sleep(1)
core.take_sample()
time.sleep(1)
core.take_sample()
time.sleep(1)

# Stop continuous recording
test_recorder.is_continuous_recording = False
core.update_test_recorder(test_recorder, ['is_continuous_recording'])

# To save a test, a title must be provided
test_recorder.title = "Python demo script test data"
core.update_test_recorder(test_recorder, ['title'])

# Save the test and get the name of the newly saved test. The name can be used to later retrieve the test data in Python
test_name = core.save_test()
print("Saved test with name: " + test_name)

print("\nDone. Use the Flight Stand user interface to explore the saved test data.")
exit()
