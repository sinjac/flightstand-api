# Compile Go files from the .proto
Note, the instructions below are for Tyto Robotics employees. Users of the API don't need to recompile the language files since they are already available in this project.

## Setup dependencies
- Install [Golang](https://golang.org/doc/install) following the official instructions for your OS. Target v.1.17.2.
- Install protoc (precompiled binary, no need to compile)
    - https://github.com/protocolbuffers/protobuf/releases/ -> for example: https://github.com/protocolbuffers/protobuf/releases/download/v3.14.0/protoc-3.14.0-win64.zip open zip and follow readme in zip.
- Install protoc-gen-go https://developers.google.com/protocol-buffers/docs/reference/go-generated

## Compile proto

1) From terminal, go to root folder of this project
2) Type `protoc --proto_path=proto --go_out=languages/go --go-grpc_out=languages/go --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative proto/flight_stand_api_v1.proto`

If you get an error, here are some solutions:
- `google/protobuf/timestamp.proto: File not found.` It means you didn't include the standard proto as instructed in the readme of the protoc zip file. More help: https://stackoverflow.com/questions/56031098/protobuf-timestamp-not-found
- `'protoc' is not recognized as an internal or external command` It means protoc binary is not in the PATH.
- `'protoc-gen-go-grpc' is not recognized as an internal or external command` Based on [this](https://github.com/golang/protobuf/issues/1053), type:
```bash
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
```